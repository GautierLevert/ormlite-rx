OrmLite Rx plugin
=================

[![Build Status](https://travis-ci.org/GautierLevert/ormlite-rx.svg?branch=develop)](https://travis-ci.org/GautierLevert/ormlite-rx)

[![codebeat badge](https://codebeat.co/badges/53cf59fb-6b2c-495a-832e-43e143df497a)](https://codebeat.co/projects/github-com-gautierlevert-ormlite-rx-develop)

[![Download](https://api.bintray.com/packages/gautierlevert/maven/ormlite-rx/images/download.svg)](https://bintray.com/gautierlevert/maven/ormlite-rx/_latestVersion)

This library aims to provide RxJava2 support to OrmLite

OrmLite
-------

OrmLite is an ORM that provides some simple, lightweight functionality for persisting Java objects to SQL databases while avoiding the complexity and overhead of more standard ORM packages.

[More information](http://ormlite.com/)

[Source code](https://github.com/j256/ormlite-core)

RxJava2
-------

RxJava2 is the Java implementation of Reactive Extensions: a library for composing asynchronous and event-based programs by using observable sequences.

It extends the observer pattern to support sequences of data/events and adds operators that allow you to compose sequences together declaratively while abstracting away concerns about things like low-level threading, synchronization, thread-safety and concurrent data structures.

[More information](http://reactivex.io/)

[Source code](https://github.com/ReactiveX/RxJava)

This plugin
-----------

This plugin offers simple Rx API for OrmLite system methods. This way you can easily bind DAO results and query with Rx in a Rx based application.

How to use it
-------------

### Installation ###

You need to add OrmLite Rx plugin in your project dependencies :

This library is hosted in a personal Maven repository on Bintray : https://bintray.com/gautierlevert/maven/ormlite-rx so you need to add this repository to your build script.

In Gradle:

```groovy
repositories {
    maven {
        url "http://dl.bintray.com/gautierlevert/maven"
    }
}
```

In Maven:

```xml
<repositories>
    <repository>
        <id>bintray-gautierlevert-maven</id>
        <name>bintray</name>
        <url>http://dl.bintray.com/gautierlevert/maven</url>
    </repository>
</repositories>
```

And then add the library dependency :


In Gradle:

```groovy
compile 'org.mybop:ormlite-rx:INSERT_LAST_VERSION_HERE'
```

In Maven:

```xml
<dependency>
    <groupId>org.mybop</groupId>
    <artifactId>ormlite-rx</artifactId>
    <version>INSERT_LAST_VERSION_HERE</version>
    <type>pom</type>
</dependency>
```

**NB:** This is a *plugin* for OrmLite so you need to add dependency to classic OrmLite library : ormlite-core and ormlite-jdbc or ormlite-android.
[See documentation](http://ormlite.com/javadoc/ormlite-core/doc-files/ormlite_1.html#Downloading)
 
### Usage ###

OmeLite-Rx usage is basically the same as OrmLite : see [OrmLite documentation](http://ormlite.com/javadoc/ormlite-core/doc-files/ormlite_toc.html) for classic usage.

#### Getting the DAO ####

Get the Dao from RxDaoManager (instead of DaoManager)

```java
RxDaoManager.createDao(connectionSource, Account.java)
```

You will get an instance of RxDao with Rx return value :

```java
accountDao.create(account).subscribe()
```

[See simple example](https://github.com/GautierLevert/ormlite-rx/tree/master/example/src/main/kotlin/org/mybop/ormlite/rx/example/simple)


#### Using custom DAO ####

You can create custom RxDao, you need to create a class extending BaseRxDao

```java
class AccountDao extends BaseRxDaoImpl<Account, Int> {

    public AccountDao(BaseDaoImpl<Account, Int> baseDaoImpl) {
        super(baseDaoImpl);
    }

    public Flowable<Account> findAllActive() {
        return queryBuilder()
            .where()
            .eq(Account.ACTIVE_COLUMN_NAME, true)
            .query();
    }

    public Completable activate(Account account) {
        return updateBuilder()
            .updateColumnValue(Account.ACTIVE_COLUMN_NAME, true)
            .where()
            .idEq(account.id)
            .update();
    }

    public Completable deactivate(Account account) {
            return updateBuilder()
                .updateColumnValue(Account.ACTIVE_COLUMN_NAME, false)
                .where()
                .idEq(account.id)
                .update();
        }
}
```

**NB:** Custom Rx DAO must have a constructor with a single BaseDaoImpl parameter and give it to the parent.

**NB:** Statement builders are not part of RxDao interface, but are accessible inside you class extending BaseRxDaoImpl cause we think this is the responsibility of the DAO to create custom queries.
This DAO must be referenced inside the entity class with a new annotation @CustomRxDao

```java
@DatabaseTable(tableName = "account")
@CustomRxDao(AccountDao.class)
public class Account {
    ...
}
```

[See custom dao example](https://github.com/GautierLevert/ormlite-rx/tree/master/example/src/main/kotlin/org/mybop/ormlite/rx/example/customdao)

#### Using transactions ####

You can use transaction calling `RxTransactionManager.callInTransaction(Completable)` to wrap your completable operation inside a transaction.

This method return another `Completable` that you can subscribe like any other.

[See custom dao example](https://github.com/GautierLevert/ormlite-rx/tree/master/example/src/main/kotlin/org/mybop/ormlite/rx/example/transaction)

### What to change in code ###

Some of the core OrmLite class, here is a list of classes you probably use in your code that must be changed for Rx variant.

| OrmLite core                              | OrmLite Rx                                     |
|-------------------------------------------|------------------------------------------------|
| com.j256.ormlite.dao.DaoManager           | org.mybop.ormlite.rx.dao.RxDaoManager          |
| com.j256.ormlite.dao.Dao                  | org.mybop.ormlite.rx.dao.RxDao                 |
| com.j256.ormlite.dao.BaseDaoImpl          | org.mybop.ormlite.rx.dao.BaseRxDaoImpl         |
| com.j256.ormlite.misc.TransactionManager  | org.mybop.ormlite.rx.misc.RxTransactionManager |

Other classes have been changed but this will probably not affect your code

| OrmLite core                            | OrmLite Rx                                                                                                                                                                    |
|-----------------------------------------|----------------------------------------------------------|
| com.j256.ormlite.stmt.Where             | org.mybop.ormlite.rx.stmt.where.RxWhere (and subclasses) |
| com.j256.ormlite.stmt.StatementBuilder  | org.mybop.ormlite.rx.stmt.RxStatementBuilder             |
| com.j256.ormlite.stmt.QueryBuilder      | org.mybop.ormlite.rx.stmt.RxQueryBuilder                 |
| com.j256.ormlite.stmt.DeleteBuilder     | org.mybop.ormlite.rx.stmt.RxDeleteBuilder                |
| com.j256.ormlite.stmt.UpdateBuilder     | org.mybop.ormlite.rx.stmt.RxUpdateBuilder                |
| com.j256.ormlite.stmt.StatementExecutor | org.mybop.ormlite.rx.stmt.RxStatementExecutor            |
