package org.mybop.ormlite.rx.stmt.publisher.prepared.select

import com.j256.ormlite.dao.BaseDaoImpl
import com.j256.ormlite.dao.DaoManager
import com.j256.ormlite.jdbc.JdbcConnectionSource
import com.j256.ormlite.table.TableUtils
import io.reactivex.Flowable
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.mybop.ormlite.rx.model.TestModel
import org.reactivestreams.Subscriber
import org.reactivestreams.Subscription

class SelectStatementPublisherIntegrationTest {

    private var connectionSource: JdbcConnectionSource? = null

    @Before
    fun setUp() {
        connectionSource = JdbcConnectionSource("jdbc:h2:mem:test;DB_CLOSE_DELAY=0")

        TableUtils.createTable(connectionSource, TestModel::class.java)
    }

    @Test
    @Suppress("unchecked_cast")
    fun subscribe_singeSubscriptionInCurrentThread() {
        val testModelDao = DaoManager.createDao(connectionSource, TestModel::class.java) as BaseDaoImpl<TestModel, Long>
        testModelDao.create((0..9L).map { TestModel(it) })

        val preparedQuery = testModelDao.queryBuilder().prepare()

        val publisher = SelectStatementPublisher<TestModel, TestModel>(
                connectionSource!!,
                testModelDao.tableInfo.tableName,
                preparedQuery,
                preparedQuery,
                null
        )

        var index = 0L
        var completed = false
        publisher.subscribe(object : Subscriber<TestModel> {
            override fun onSubscribe(s: Subscription) {
                s.request(16)
            }

            override fun onNext(t: TestModel) {
                assertEquals(index, t.id)
                index++
            }

            override fun onComplete() {
                completed = true
            }

            override fun onError(t: Throwable) {
                fail()
            }
        })
        assertNotEquals(index, 0)
        assertTrue(completed)
    }

    @Test
    @Suppress("unchecked_cast")
    fun subscribe_multipleSubscriptionConcurrent() {
        val testModelDao = DaoManager.createDao(connectionSource, TestModel::class.java) as BaseDaoImpl<TestModel, Long>
        testModelDao.create((0..255L).map { TestModel(it) })

        val preparedQuery = testModelDao.queryBuilder().prepare()

        val flowable1 = Flowable.fromPublisher(SelectStatementPublisher<TestModel, TestModel>(
                connectionSource!!,
                testModelDao.tableInfo.tableName,
                preparedQuery,
                preparedQuery,
                null
        )).subscribeOn(Schedulers.io())

        val flowable2 = Flowable.fromPublisher(SelectStatementPublisher<TestModel, TestModel>(
                connectionSource!!,
                testModelDao.tableInfo.tableName,
                preparedQuery,
                preparedQuery,
                null
        )).subscribeOn(Schedulers.io())

        val count = flowable1
                .zipWith(flowable2, BiFunction { o1: TestModel, o2: TestModel ->
                    o1.id == o2.id
                })
                .filter { it }
                .count()
                .blockingGet()

        assertEquals(count, 256)
    }

    @After
    fun tearDown() {
        connectionSource?.closeQuietly()
    }
}
