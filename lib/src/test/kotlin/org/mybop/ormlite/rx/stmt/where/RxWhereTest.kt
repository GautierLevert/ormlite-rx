package org.mybop.ormlite.rx.stmt.where

import com.j256.ormlite.stmt.PreparedStmt
import com.j256.ormlite.stmt.QueryBuilder
import com.j256.ormlite.stmt.SelectArg
import com.j256.ormlite.stmt.Where
import com.nhaarman.mockito_kotlin.*
import io.reactivex.Single
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.mybop.ormlite.rx.dao.BaseRxDaoImpl
import org.mybop.ormlite.rx.model.TestModel
import org.mybop.ormlite.rx.stmt.RxQueryBuilder

@RunWith(MockitoJUnitRunner::class)
class RxWhereTest {

    @Mock
    private lateinit var dao: BaseRxDaoImpl<TestModel, Long>

    @Test
    fun and() {
        val where = mock<Where<TestModel, Long>>()

        val rxWhere = StubRxWhere(where)

        rxWhere.and()

        verify(where).and()
    }

    @Test
    fun and_where() {
        val where = mock<Where<TestModel, Long>>()

        val firstWhere = mock<Where<TestModel, Long>>()
        val firstRxWhere = StubRxWhere(firstWhere)

        val secondWhere = mock<Where<TestModel, Long>>()
        val secondRxWhere = StubRxWhere(secondWhere)

        val thirdWhere = mock<Where<TestModel, Long>>()
        val thirdRxWhere = StubRxWhere(thirdWhere)

        val rxWhere = StubRxWhere(where)

        rxWhere.and(firstRxWhere, secondRxWhere, thirdRxWhere)

        verify(where).and(eq(firstWhere), eq(secondWhere), eq(thirdWhere))
    }

    @Test
    fun and_numClauses() {
        val where = mock<Where<TestModel, Long>>()

        val rxWhere = StubRxWhere(where)

        rxWhere.and(1)

        verify(where).and(eq(1))
    }

    @Test
    fun between() {
        val where = mock<Where<TestModel, Long>>()

        val rxWhere = StubRxWhere(where)

        rxWhere.between("field", -10, 10)

        verify(where).between(eq("field"), eq(-10), eq(10))
    }

    @Test
    fun eq() {
        val where = mock<Where<TestModel, Long>>()

        val rxWhere = StubRxWhere(where)

        rxWhere.eq("field", 42)

        verify(where).eq(eq("field"), eq(42))
    }

    @Test
    fun ge() {
        val where = mock<Where<TestModel, Long>>()

        val rxWhere = StubRxWhere(where)

        rxWhere.ge("field", 42)

        verify(where).ge(eq("field"), eq(42))
    }

    @Test
    fun gt() {
        val where = mock<Where<TestModel, Long>>()

        val rxWhere = StubRxWhere(where)

        rxWhere.gt("field", 42)

        verify(where).gt(eq("field"), eq(42))
    }

    @Test
    fun in_iterable() {
        val where = mock<Where<TestModel, Long>>()

        val rxWhere = StubRxWhere(where)

        rxWhere.`in`("field", listOf(-10, 10))

        verify(where).`in`(eq("field"), eq(listOf(-10, 10)))
    }

    @Test
    fun notIn_iterable() {
        val where = mock<Where<TestModel, Long>>()

        val rxWhere = StubRxWhere(where)

        rxWhere.notIn("field", listOf(-10, 10))

        verify(where).notIn(eq("field"), eq(listOf(-10, 10)))
    }

    @Test
    fun in_vararg() {
        val where = mock<Where<TestModel, Long>>()

        val rxWhere = StubRxWhere(where)

        rxWhere.`in`("field", -10, 10)

        verify(where).`in`(eq("field"), eq(-10), eq(10))
    }

    @Test
    fun notIn_vararg() {
        val where = mock<Where<TestModel, Long>>()

        val rxWhere = StubRxWhere(where)

        rxWhere.notIn("field", -10, 10)

        verify(where).notIn(eq("field"), eq(-10), eq(10))
    }

    @Test
    fun in_queryBuilder() {
        val where = mock<Where<TestModel, Long>>()

        val rxWhere = StubRxWhere(where)

        val subQueryBuilder = mock<QueryBuilder<TestModel, Long>>()
        val subRxQueryBuilder = RxQueryBuilder(subQueryBuilder, dao)

        rxWhere.`in`("field", subRxQueryBuilder)
        verify(where).`in`(eq("field"), eq(subQueryBuilder))
    }

    @Test
    fun notIn_queryBuilder() {
        val where = mock<Where<TestModel, Long>>()

        val rxWhere = StubRxWhere(where)

        val subQueryBuilder = mock<QueryBuilder<TestModel, Long>>()
        val subRxQueryBuilder = RxQueryBuilder(subQueryBuilder, dao)

        rxWhere.notIn("field", subRxQueryBuilder)
        verify(where).notIn(eq("field"), eq(subQueryBuilder))
    }

    @Test
    fun exists() {
        val where = mock<Where<TestModel, Long>>()

        val rxWhere = StubRxWhere(where)

        val subQueryBuilder = mock<QueryBuilder<TestModel, Long>>()
        val subRxQueryBuilder = RxQueryBuilder(subQueryBuilder, dao)

        rxWhere.exists(subRxQueryBuilder)
        verify(where).exists(eq(subQueryBuilder))
    }

    @Test
    fun isNull() {
        val where = mock<Where<TestModel, Long>>()

        val rxWhere = StubRxWhere(where)

        rxWhere.isNull("field")

        verify(where).isNull(eq("field"))
    }

    @Test
    fun isNotNull() {
        val where = mock<Where<TestModel, Long>>()

        val rxWhere = StubRxWhere(where)

        rxWhere.isNotNull("field")

        verify(where).isNotNull(eq("field"))
    }

    @Test
    fun le() {
        val where = mock<Where<TestModel, Long>>()

        val rxWhere = StubRxWhere(where)

        rxWhere.le("field", 42)

        verify(where).le(eq("field"), eq(42))
    }

    @Test
    fun lt() {
        val where = mock<Where<TestModel, Long>>()

        val rxWhere = StubRxWhere(where)

        rxWhere.lt("field", 42)

        verify(where).lt(eq("field"), eq(42))
    }

    @Test
    fun like() {
        val where = mock<Where<TestModel, Long>>()

        val rxWhere = StubRxWhere(where)

        rxWhere.like("field", 42)

        verify(where).like(eq("field"), eq(42))
    }

    @Test
    fun ne() {
        val where = mock<Where<TestModel, Long>>()

        val rxWhere = StubRxWhere(where)

        rxWhere.ne("field", 42)

        verify(where).ne(eq("field"), eq(42))
    }

    @Test
    operator fun not() {
        val where = mock<Where<TestModel, Long>>()

        val rxWhere = StubRxWhere(where)

        rxWhere.not()
        verify(where).not()
    }

    @Test
    fun not_where() {
        val where = mock<Where<TestModel, Long>>()

        val subWhere = mock<Where<TestModel, Long>>()
        val subRxWhere = StubRxWhere(subWhere)

        val rxWhere = StubRxWhere(where)

        rxWhere.not(subRxWhere)

        verify(where).not(eq(subWhere))
    }

    @Test
    fun or() {
        val where = mock<Where<TestModel, Long>>()

        val rxWhere = StubRxWhere(where)

        rxWhere.or()
        verify(where).or()
    }

    @Test
    fun or_where() {
        val where = mock<Where<TestModel, Long>>()

        val firstWhere = mock<Where<TestModel, Long>>()
        val firstRxWhere = StubRxWhere(firstWhere)

        val secondWhere = mock<Where<TestModel, Long>>()
        val secondRxWhere = StubRxWhere(secondWhere)

        val thirdWhere = mock<Where<TestModel, Long>>()
        val thirdRxWhere = StubRxWhere(thirdWhere)

        val rxWhere = StubRxWhere(where)

        rxWhere.or(firstRxWhere, secondRxWhere, thirdRxWhere)

        verify(where).or(eq(firstWhere), eq(secondWhere), eq(thirdWhere))
    }

    @Test
    fun or_numClauses() {
        val where = mock<Where<TestModel, Long>>()

        val rxWhere = StubRxWhere(where)

        rxWhere.or(1)

        verify(where).or(eq(1))
    }

    @Test
    fun idEq() {
        val where = mock<Where<TestModel, Long>>()

        val rxWhere = StubRxWhere(where)

        rxWhere.idEq(1)

        verify(where).idEq(eq(1))
    }

    @Test
    fun raw() {
        val where = mock<Where<TestModel, Long>>()

        val rxWhere = StubRxWhere(where)

        rxWhere.raw("raw statement", SelectArg("arg1", "val1"), SelectArg("arg2", "val2"))

        verify(where).raw(eq("raw statement"), refEq(SelectArg("arg1", "val1")), refEq(SelectArg("arg2", "val2")))
    }

    @Test
    fun rawComparison() {
        val where = mock<Where<TestModel, Long>>()

        val rxWhere = StubRxWhere(where)

        rxWhere.rawComparison("field", "operator", "value")

        verify(where).rawComparison(eq("field"), eq("operator"), eq("value"))
    }

    @Test
    fun reset() {
        val where = mock<Where<TestModel, Long>>()

        val rxWhere = StubRxWhere(where)

        rxWhere.reset()

        verify(where).reset()
    }

    @Test
    fun getStatement() {
        val where = mock<Where<TestModel, Long>> {
            on { statement } doReturn "statement"
        }

        val rxWhere = StubRxWhere(where)

        rxWhere.getStatement().subscribe { statement ->
            assertThat(statement).isEqualTo("statement")
        }
    }

    private class StubRxWhere<T, ID>(where: Where<T, ID>) : RxWhere<T, ID, StubRxWhere<T, ID>>(where) {
        override fun prepare(): Single<out PreparedStmt<T>> {
            return Single.just(mock())
        }
    }
}
