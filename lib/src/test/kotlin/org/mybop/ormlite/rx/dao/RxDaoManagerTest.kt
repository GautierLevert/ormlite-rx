package org.mybop.ormlite.rx.dao

import com.j256.ormlite.dao.BaseDaoImpl
import com.j256.ormlite.dao.Dao
import com.j256.ormlite.dao.DaoManager
import com.j256.ormlite.db.DatabaseType
import com.j256.ormlite.support.ConnectionSource
import com.j256.ormlite.table.TableInfo
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.eq
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import org.mybop.ormlite.rx.model.TestModel
import org.powermock.api.mockito.PowerMockito
import org.powermock.core.classloader.annotations.PrepareForTest
import org.powermock.modules.junit4.PowerMockRunner

@RunWith(PowerMockRunner::class)
@PrepareForTest(DaoManager::class)
class RxDaoManagerTest {

    @Test
    @Suppress("unchecked_cast")
    fun createDao_byClass() {
        val databaseType = PowerMockito.mock(DatabaseType::class.java)

        val connectionSource = PowerMockito.mock(ConnectionSource::class.java)
        PowerMockito.`when`<DatabaseType>({ connectionSource.databaseType }).doReturn(databaseType)

        val tableInfo = PowerMockito.mock(TableInfo::class.java) as TableInfo<TestModel, Long>

        val mockDao = PowerMockito.mock(BaseDaoImpl::class.java) as BaseDaoImpl<TestModel, Long>
        PowerMockito.`when`<Class<TestModel>>({ mockDao.dataClass }).doReturn(TestModel::class.java)
        PowerMockito.`when`<ConnectionSource>({ mockDao.connectionSource }).doReturn(connectionSource)
        PowerMockito.`when`<TableInfo<TestModel, Long>>({ mockDao.tableInfo }).doReturn(tableInfo)

        PowerMockito.mockStatic(DaoManager::class.java)
        PowerMockito.`when`(DaoManager.createDao(any(), eq(TestModel::class.java))).doReturn(mockDao as Dao<TestModel, Long>)

        val rxDao = RxDaoManager.createDao<TestModel, Long>(PowerMockito.mock(ConnectionSource::class.java), TestModel::class.java)

        assertThat(rxDao).isExactlyInstanceOf(BaseRxDaoImpl::class.java)
        assertThat((rxDao as BaseRxDaoImpl<TestModel, Long>).dao).isEqualTo(mockDao)
    }

    @Test
    fun lookupDao_byClass_notRegistered() {
        val rxDao = RxDaoManager.lookupDao<TestModel, Long>(PowerMockito.mock(ConnectionSource::class.java), TestModel::class.java)

        assertThat(rxDao).isNull() // initially lookup give null
    }

    @Test
    @Suppress("unchecked_cast")
    fun lookupDao_byClass_badlyRegistered() {
        val databaseType = PowerMockito.mock(DatabaseType::class.java)

        val connectionSource = PowerMockito.mock(ConnectionSource::class.java)
        PowerMockito.`when`<DatabaseType>({ connectionSource.databaseType }).doReturn(databaseType)

        val tableInfo = PowerMockito.mock(TableInfo::class.java) as TableInfo<TestModel, Long>

        val mockDao = PowerMockito.mock(BaseDaoImpl::class.java) as BaseDaoImpl<TestModel, Long>
        PowerMockito.`when`<Class<TestModel>>({ mockDao.dataClass }).doReturn(TestModel::class.java)
        PowerMockito.`when`<ConnectionSource>({ mockDao.connectionSource }).doReturn(connectionSource)
        PowerMockito.`when`<TableInfo<TestModel, Long>>({ mockDao.tableInfo }).doReturn(tableInfo)

        PowerMockito.mockStatic(DaoManager::class.java)
        PowerMockito.`when`(DaoManager.lookupDao(any(), eq(TestModel::class.java))).doReturn(mockDao as Dao<TestModel, Long>)

        val rxDao = RxDaoManager.lookupDao<TestModel, Long>(PowerMockito.mock(ConnectionSource::class.java), TestModel::class.java)

        assertThat(rxDao).isNull() // initially lookup give null
    }

    @Test
    @Suppress("unchecked_cast")
    fun lookupDao_byClass_afterCreate() {
        val databaseType = PowerMockito.mock(DatabaseType::class.java)

        val connectionSource = PowerMockito.mock(ConnectionSource::class.java)
        PowerMockito.`when`<DatabaseType>({ connectionSource.databaseType }).doReturn(databaseType)

        val tableInfo = PowerMockito.mock(TableInfo::class.java) as TableInfo<TestModel, Long>

        val mockDao = PowerMockito.mock(BaseDaoImpl::class.java) as BaseDaoImpl<TestModel, Long>
        PowerMockito.`when`<Class<TestModel>>({ mockDao.dataClass }).doReturn(TestModel::class.java)
        PowerMockito.`when`<ConnectionSource>({ mockDao.connectionSource }).doReturn(connectionSource)
        PowerMockito.`when`<TableInfo<TestModel, Long>>({ mockDao.tableInfo }).doReturn(tableInfo)

        PowerMockito.mockStatic(DaoManager::class.java)
        PowerMockito.`when`(DaoManager.createDao(any(), eq(TestModel::class.java))).doReturn(mockDao as Dao<TestModel, Long>)

        val expected = RxDaoManager.createDao<TestModel, Long>(PowerMockito.mock(ConnectionSource::class.java), TestModel::class.java)

        PowerMockito.`when`(DaoManager.lookupDao(any(), eq(TestModel::class.java))).doReturn(mockDao as Dao<TestModel, Long>)

        val rxDao = RxDaoManager.lookupDao<TestModel, Long>(PowerMockito.mock(ConnectionSource::class.java), TestModel::class.java)

        assertThat(rxDao).isExactlyInstanceOf(BaseRxDaoImpl::class.java)
        assertThat(rxDao).isEqualTo(expected)
        assertThat((rxDao as BaseRxDaoImpl<TestModel, Long>).dao).isEqualTo(mockDao)
    }
}
