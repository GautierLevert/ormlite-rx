package org.mybop.ormlite.rx.stmt

import com.j256.ormlite.dao.ObjectCache
import com.j256.ormlite.stmt.GenericRowMapper
import com.j256.ormlite.support.ConnectionSource
import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import org.junit.Test
import org.mybop.ormlite.rx.stmt.publisher.PublisherBuilder
import org.mybop.ormlite.rx.stmt.publisher.compiled.execute.RawExecutePublisher
import org.mybop.ormlite.rx.stmt.publisher.compiled.select.RawSelectPublisher

class RxRawStatementExecutorTest {

    @Test
    fun queryRaw() {
        val connectionSource = mock<ConnectionSource>()
        val query = ""
        val arguments = arrayOf<String>()
        val rowMapper = mock<GenericRowMapper<Any>>()
        val objectCache = mock<ObjectCache>()
        val publisherBuilder = mock<PublisherBuilder<Any, Any>> {
            on { selectRaw(connectionSource, query, arguments, rowMapper, objectCache, null) } doReturn mock<RawSelectPublisher<Any>>()
        }
        val rxStatementExecutor = RxRawStatementExecutor(
                publisherBuilder
        )

        rxStatementExecutor.queryRaw(connectionSource, query, arguments, rowMapper, objectCache)
        verify(publisherBuilder).selectRaw(connectionSource, query, arguments, rowMapper, objectCache, null)
    }

    @Test
    fun queryRawFirst() {
        val connectionSource = mock<ConnectionSource>()
        val query = ""
        val arguments = arrayOf<String>()
        val rowMapper = mock<GenericRowMapper<Any>>()
        val objectCache = mock<ObjectCache>()
        val publisherBuilder = mock<PublisherBuilder<Any, Any>> {
            on { selectRaw(connectionSource, query, arguments, rowMapper, objectCache, 1) } doReturn mock<RawSelectPublisher<Any>>()
        }
        val rxStatementExecutor = RxRawStatementExecutor(
                publisherBuilder
        )

        rxStatementExecutor.queryRawFirst(connectionSource, query, arguments, rowMapper, objectCache)
        verify(publisherBuilder).selectRaw(connectionSource, query, arguments, rowMapper, objectCache, 1)
    }

    @Test
    fun executeRaw() {
        val connectionSource = mock<ConnectionSource>()
        val query = ""
        val arguments = arrayOf<String>()
        val publisherBuilder = mock<PublisherBuilder<Any, Any>> {
            on { executeRaw(connectionSource, query, arguments) } doReturn mock<RawExecutePublisher>()
        }
        val rxStatementExecutor = RxRawStatementExecutor(
                publisherBuilder
        )

        rxStatementExecutor.executeRaw(connectionSource, query, arguments)
        verify(publisherBuilder).executeRaw(connectionSource, query, arguments)
    }

    @Test
    fun updateRaw() {
        val connectionSource = mock<ConnectionSource>()
        val query = ""
        val arguments = arrayOf<String>()
        val publisherBuilder = mock<PublisherBuilder<Any, Any>> {
            on { updateRaw(connectionSource, query, arguments) } doReturn mock<RawExecutePublisher>()
        }
        val rxStatementExecutor = RxRawStatementExecutor(
                publisherBuilder
        )

        rxStatementExecutor.updateRaw(connectionSource, query, arguments)
        verify(publisherBuilder).updateRaw(connectionSource, query, arguments)
    }
}
