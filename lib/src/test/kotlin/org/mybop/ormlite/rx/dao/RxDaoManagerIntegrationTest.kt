package org.mybop.ormlite.rx.dao

import com.j256.ormlite.dao.BaseDaoImpl
import com.j256.ormlite.field.DatabaseField
import com.j256.ormlite.jdbc.JdbcConnectionSource
import com.nhaarman.mockito_kotlin.mock
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test

class RxDaoManagerIntegrationTest {

    @Before
    fun setUp() {
        RxDaoManager.clearCache()
    }

    @Test
    fun createDao_customDao() {
        val connectionSource = JdbcConnectionSource("jdbc:h2:mem:test;DB_CLOSE_DELAY=0")
        val dao = RxDaoManager.createDao<Foo, Int>(connectionSource, Foo::class.java)
        assertThat(dao).isExactlyInstanceOf(FooDao::class.java)
    }

    @Test(expected = IllegalStateException::class)
    fun createDao_wrongConstructor() {
        val connectionSource = JdbcConnectionSource("jdbc:h2:mem:test;DB_CLOSE_DELAY=0")
        val dao = RxDaoManager.createDao<Bar, Int>(connectionSource, Bar::class.java)
        assertThat(dao).isExactlyInstanceOf(BarDao::class.java)
    }

    @CustomRxDao(FooDao::class)
    internal class Foo {
        @DatabaseField(generatedId = true)
        internal var id: Int = 0
    }

    internal class FooDao(dao: BaseDaoImpl<Foo, Int>) : BaseRxDaoImpl<Foo, Int>(dao)

    @CustomRxDao(BarDao::class)
    internal class Bar {
        @DatabaseField(generatedId = true)
        internal var id: Int = 0
    }

    internal class BarDao : BaseRxDaoImpl<Bar, Int>(mock())
}
