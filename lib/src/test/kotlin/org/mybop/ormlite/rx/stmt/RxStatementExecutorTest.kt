package org.mybop.ormlite.rx.stmt

import com.j256.ormlite.dao.ObjectCache
import com.j256.ormlite.stmt.PreparedDelete
import com.j256.ormlite.stmt.PreparedQuery
import com.j256.ormlite.stmt.PreparedUpdate
import com.j256.ormlite.stmt.mapped.*
import com.j256.ormlite.support.ConnectionSource
import com.nhaarman.mockito_kotlin.*
import io.reactivex.Single
import org.junit.Test
import org.mybop.ormlite.rx.stmt.publisher.PublisherBuilder
import org.mybop.ormlite.rx.stmt.publisher.mapped.create.MappedCreatePublisher
import org.mybop.ormlite.rx.stmt.publisher.mapped.delete.MappedDeletePublisher
import org.mybop.ormlite.rx.stmt.publisher.mapped.deletecollection.MappedDeleteCollectionPublisher
import org.mybop.ormlite.rx.stmt.publisher.mapped.refresh.MappedRefreshPublisher
import org.mybop.ormlite.rx.stmt.publisher.mapped.update.MappedUpdatePublisher
import org.mybop.ormlite.rx.stmt.publisher.mapped.updateid.MappedUpdateIdPublisher
import org.mybop.ormlite.rx.stmt.publisher.prepared.select.SelectStatementPublisher
import org.mybop.ormlite.rx.stmt.publisher.prepared.ud.UpdateDeleteStatementPublisher
import org.reactivestreams.Subscriber
import org.reactivestreams.Subscription

class RxStatementExecutorTest {

    @Test
    fun query() {
        val connectionSource = mock<ConnectionSource>()
        val preparedQuery = mock<PreparedQuery<Any>>()
        val objectCache = mock<ObjectCache>()
        val publisherBuilder = mock<PublisherBuilder<Any, Any>> {
            on { select(connectionSource, preparedQuery, objectCache) } doReturn mock<SelectStatementPublisher<Any, Any>>()
        }
        val rxStatementExecutor = RxStatementExecutor(
                mock(),
                publisherBuilder
        )

        rxStatementExecutor.query(connectionSource, preparedQuery, objectCache)
        verify(publisherBuilder).select(connectionSource, preparedQuery, objectCache)
    }

    @Test
    fun queryForFirst() {
        val connectionSource = mock<ConnectionSource>()
        val preparedQuery = mock<PreparedQuery<Any>>()
        val objectCache = mock<ObjectCache>()
        val publisherBuilder = mock<PublisherBuilder<Any, Any>> {
            on { select(connectionSource, preparedQuery, objectCache, 1) } doReturn mock<SelectStatementPublisher<Any, Any>>()
        }
        val rxStatementExecutor = RxStatementExecutor(
                mock(),
                publisherBuilder
        )

        rxStatementExecutor.queryForFirst(connectionSource, preparedQuery, objectCache)
        verify(publisherBuilder).select(connectionSource, preparedQuery, objectCache, 1)
    }

    @Test
    fun queryForCount() {
        val connectionSource = mock<ConnectionSource>()
        val preparedQuery = mock<PreparedQuery<Any>>()
        val publisherBuilder = mock<PublisherBuilder<Any, Any>> {
            on { count(connectionSource, preparedQuery) } doReturn mock<SelectStatementPublisher<Any, Long>>()
        }
        val rxStatementExecutor = RxStatementExecutor(
                mock(),
                publisherBuilder
        )

        rxStatementExecutor.queryForCount(connectionSource, preparedQuery)
        verify(publisherBuilder).count(connectionSource, preparedQuery)
    }

    @Test
    fun create() {
        val data = Any()
        val connectionSource = mock<ConnectionSource>()
        val objectCache = mock<ObjectCache>()
        val mappedCreate = mock<MappedCreate<Any, Any>>()
        val commonlyUsedMappedStatements = mock<CommonlyUsedMappedStatements<Any, Any>> {
            on { it.mappedCreate } doReturn Single.just(mappedCreate)
        }
        val mappedCreatePublisher = mock<MappedCreatePublisher<Any, Any>> {
            @Suppress("unchecked_cast")
            on { it.subscribe(any()) } doAnswer {
                val subscriber = it.arguments[0] as Subscriber<Int>
                subscriber.onSubscribe(mock())
                subscriber.onNext(1)
                subscriber.onComplete()
            }
        }
        val publisherBuilder = mock<PublisherBuilder<Any, Any>> {
            on { it.create(connectionSource, mappedCreate, data, objectCache) } doReturn mappedCreatePublisher
        }
        val rxStatementExecutor = RxStatementExecutor(
                commonlyUsedMappedStatements,
                publisherBuilder
        )

        rxStatementExecutor.create(connectionSource, data, objectCache).blockingAwait()
        verify(publisherBuilder).create(connectionSource, mappedCreate, data, objectCache)
    }

    @Test
    fun update() {
        val data = Any()
        val connectionSource = mock<ConnectionSource>()
        val objectCache = mock<ObjectCache>()
        val mappedUpdate = mock<MappedUpdate<Any, Any>>()
        val commonlyUsedMappedStatements = mock<CommonlyUsedMappedStatements<Any, Any>> {
            on { it.mappedUpdate } doReturn Single.just(mappedUpdate)
        }
        val mappedUpdatePublisher = mock<MappedUpdatePublisher<Any, Any>> {
            @Suppress("unchecked_cast")
            on { it.subscribe(any()) } doAnswer {
                val subscriber = it.arguments[0] as Subscriber<Int>
                subscriber.onSubscribe(mock())
                subscriber.onNext(1)
                subscriber.onComplete()
            }
        }
        val publisherBuilder = mock<PublisherBuilder<Any, Any>> {
            on { it.update(connectionSource, mappedUpdate, data, objectCache) } doReturn mappedUpdatePublisher
        }
        val rxStatementExecutor = RxStatementExecutor(
                commonlyUsedMappedStatements,
                publisherBuilder
        )

        rxStatementExecutor.update(connectionSource, data, objectCache).blockingAwait()
        verify(publisherBuilder).update(connectionSource, mappedUpdate, data, objectCache)
    }

    @Test
    fun updateId() {
        val data = Any()
        val id = Any()
        val connectionSource = mock<ConnectionSource>()
        val objectCache = mock<ObjectCache>()
        val mappedUpdate = mock<MappedUpdateId<Any, Any>>()
        val commonlyUsedMappedStatements = mock<CommonlyUsedMappedStatements<Any, Any>> {
            on { it.mappedUpdateId } doReturn Single.just(mappedUpdate)
        }
        val mappedUpdateIdPublisher = mock<MappedUpdateIdPublisher<Any, Any>> {
            @Suppress("unchecked_cast")
            on { it.subscribe(any()) } doAnswer {
                val subscriber = it.arguments[0] as Subscriber<Int>
                subscriber.onSubscribe(mock())
                subscriber.onNext(1)
                subscriber.onComplete()
            }
        }
        val publisherBuilder = mock<PublisherBuilder<Any, Any>> {
            on { it.updateId(connectionSource, mappedUpdate, data, id, objectCache) } doReturn mappedUpdateIdPublisher
        }
        val rxStatementExecutor = RxStatementExecutor(
                commonlyUsedMappedStatements,
                publisherBuilder
        )

        rxStatementExecutor.updateId(connectionSource, data, id, objectCache).blockingAwait()
        verify(publisherBuilder).updateId(connectionSource, mappedUpdate, data, id, objectCache)
    }

    @Test
    fun update_prepared() {
        val connectionSource = mock<ConnectionSource>()
        val preparedUpdate = mock<PreparedUpdate<Any>>()
        val publisherBuilder = mock<PublisherBuilder<Any, Any>> {
            on { it.preparedUpdate(connectionSource, preparedUpdate) } doReturn mock<UpdateDeleteStatementPublisher<Any>>()
        }
        val rxStatementExecutor = RxStatementExecutor(
                mock(),
                publisherBuilder
        )

        rxStatementExecutor.update(connectionSource, preparedUpdate)
        verify(publisherBuilder).preparedUpdate(connectionSource, preparedUpdate)
    }

    @Test
    fun refresh() {
        val data = Any()
        val connectionSource = mock<ConnectionSource>()
        val objectCache = mock<ObjectCache>()
        val mappedRefresh = mock<MappedRefresh<Any, Any>>()
        val commonlyUsedMappedStatements = mock<CommonlyUsedMappedStatements<Any, Any>> {
            on { it.mappedRefresh } doReturn Single.just(mappedRefresh)
        }
        val mappedRefreshPublisher = mock<MappedRefreshPublisher<Any, Any>> {
            @Suppress("unchecked_cast")
            on { it.subscribe(any()) } doAnswer {
                val subscriber = it.arguments[0] as Subscriber<Any>
                subscriber.onSubscribe(mock())
                subscriber.onNext(data)
                subscriber.onComplete()
            }
        }
        val publisherBuilder = mock<PublisherBuilder<Any, Any>> {
            on { it.refresh(connectionSource, mappedRefresh, data, objectCache) } doReturn mappedRefreshPublisher
        }
        val rxStatementExecutor = RxStatementExecutor(
                commonlyUsedMappedStatements,
                publisherBuilder
        )

        rxStatementExecutor.refresh(connectionSource, data, objectCache).blockingGet()
        verify(publisherBuilder).refresh(connectionSource, mappedRefresh, data, objectCache)
    }

    @Test
    fun delete() {
        val data = Any()
        val connectionSource = mock<ConnectionSource>()
        val objectCache = mock<ObjectCache>()
        val mappedDelete = mock<MappedDelete<Any, Any>>()
        val commonlyUsedMappedStatements = mock<CommonlyUsedMappedStatements<Any, Any>> {
            on { it.mappedDelete } doReturn Single.just(mappedDelete)
        }
        val mappedDeletePublisher = mock<MappedDeletePublisher<Any, Any>> {
            @Suppress("unchecked_cast")
            on { it.subscribe(any()) } doAnswer {
                val subscriber = it.arguments[0] as Subscriber<Int>
                subscriber.onSubscribe(mock())
                subscriber.onNext(1)
                subscriber.onComplete()
            }
        }
        val publisherBuilder = mock<PublisherBuilder<Any, Any>> {
            on { it.delete(connectionSource, mappedDelete, data, objectCache) } doReturn mappedDeletePublisher
        }
        val rxStatementExecutor = RxStatementExecutor(
                commonlyUsedMappedStatements,
                publisherBuilder
        )

        rxStatementExecutor.delete(connectionSource, data, objectCache).blockingAwait()
        verify(publisherBuilder).delete(connectionSource, mappedDelete, data, objectCache)
    }

    @Test
    fun deleteById() {
        val id = Any()
        val connectionSource = mock<ConnectionSource>()
        val objectCache = mock<ObjectCache>()
        val mappedDelete = mock<MappedDelete<Any, Any>>()
        val commonlyUsedMappedStatements = mock<CommonlyUsedMappedStatements<Any, Any>> {
            on { it.mappedDelete } doReturn Single.just(mappedDelete)
        }
        val mappedDeletePublisher = mock<MappedDeletePublisher<Any, Any>> {
            @Suppress("unchecked_cast")
            on { it.subscribe(any()) } doAnswer {
                val subscriber = it.arguments[0] as Subscriber<Int>
                subscriber.onSubscribe(mock())
                subscriber.onNext(1)
                subscriber.onComplete()
            }
        }
        val publisherBuilder = mock<PublisherBuilder<Any, Any>> {
            on { it.deleteById(connectionSource, mappedDelete, id, objectCache) } doReturn mappedDeletePublisher
        }
        val rxStatementExecutor = RxStatementExecutor(
                commonlyUsedMappedStatements,
                publisherBuilder
        )

        rxStatementExecutor.deleteById(connectionSource, id, objectCache).blockingAwait()
        verify(publisherBuilder).deleteById(connectionSource, mappedDelete, id, objectCache)
    }

    @Test
    fun deleteObjects() {
        val data = listOf(Any())
        val connectionSource = mock<ConnectionSource>()
        val objectCache = mock<ObjectCache>()
        val mappedDelete = mock<MappedDelete<Any, Any>>()
        val commonlyUsedMappedStatements = mock<CommonlyUsedMappedStatements<Any, Any>> {
            on { it.mappedDelete } doReturn Single.just(mappedDelete)
        }
        val mappedDeleteCollectionPublisher = mock<MappedDeleteCollectionPublisher<Any, Any>> {
            @Suppress("unchecked_cast")
            on { it.subscribe(any()) } doAnswer {
                val subscriber = it.arguments[0] as Subscriber<Int>
                subscriber.onSubscribe(mock())
                subscriber.onNext(1)
                subscriber.onComplete()
            }
        }
        val publisherBuilder = mock<PublisherBuilder<Any, Any>> {
            on { it.deleteObjects(connectionSource, data, objectCache) } doReturn mappedDeleteCollectionPublisher
        }
        val rxStatementExecutor = RxStatementExecutor(
                commonlyUsedMappedStatements,
                publisherBuilder
        )

        rxStatementExecutor.deleteObjects(connectionSource, data, objectCache).blockingAwait()
        verify(publisherBuilder).deleteObjects(connectionSource, data, objectCache)
    }

    @Test
    fun deleteIds() {
        val ids = listOf(Any())
        val connectionSource = mock<ConnectionSource>()
        val objectCache = mock<ObjectCache>()
        val mappedDelete = mock<MappedDelete<Any, Any>>()
        val commonlyUsedMappedStatements = mock<CommonlyUsedMappedStatements<Any, Any>> {
            on { it.mappedDelete } doReturn Single.just(mappedDelete)
        }
        val mappedDeleteCollectionPublisher = mock<MappedDeleteCollectionPublisher<Any, Any>> {
            @Suppress("unchecked_cast")
            on { it.subscribe(any()) } doAnswer {
                val subscriber = it.arguments[0] as Subscriber<Int>
                subscriber.onSubscribe(mock())
                subscriber.onNext(1)
                subscriber.onComplete()
            }
        }
        val publisherBuilder = mock<PublisherBuilder<Any, Any>> {
            on { it.deleteIds(connectionSource, ids, objectCache) } doReturn mappedDeleteCollectionPublisher
        }
        val rxStatementExecutor = RxStatementExecutor(
                commonlyUsedMappedStatements,
                publisherBuilder
        )

        rxStatementExecutor.deleteIds(connectionSource, ids, objectCache).blockingAwait()
        verify(publisherBuilder).deleteIds(connectionSource, ids, objectCache)
    }

    @Test
    fun delete_publisher() {
        val connectionSource = mock<ConnectionSource>()
        val preparedDelete = mock<PreparedDelete<Any>>()
        val publisherBuilder = mock<PublisherBuilder<Any, Any>> {
            on { it.preparedDelete(connectionSource, preparedDelete) } doReturn mock<UpdateDeleteStatementPublisher<Any>>()
        }
        val rxStatementExecutor = RxStatementExecutor(
                mock(),
                publisherBuilder
        )

        rxStatementExecutor.delete(connectionSource, preparedDelete)
        verify(publisherBuilder).preparedDelete(connectionSource, preparedDelete)
    }
}
