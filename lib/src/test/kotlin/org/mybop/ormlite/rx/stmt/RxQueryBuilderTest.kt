package org.mybop.ormlite.rx.stmt

import com.j256.ormlite.stmt.PreparedQuery
import com.j256.ormlite.stmt.QueryBuilder
import com.j256.ormlite.stmt.SelectArg
import com.j256.ormlite.stmt.Where
import com.nhaarman.mockito_kotlin.*
import io.reactivex.Flowable
import io.reactivex.Maybe
import io.reactivex.Single
import org.assertj.core.api.Assertions
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.mybop.ormlite.rx.dao.BaseRxDaoImpl
import org.mybop.ormlite.rx.model.TestModel
import org.mybop.ormlite.rx.stmt.where.RxQueryWhere

@RunWith(MockitoJUnitRunner::class)
class RxQueryBuilderTest {

    @Mock
    private lateinit var queryBuilder: QueryBuilder<TestModel, Long>

    @Mock
    private lateinit var rxDao: BaseRxDaoImpl<TestModel, Long>

    private lateinit var rxQueryBuilder: RxQueryBuilder<TestModel, Long>

    @Before
    fun setUp() {
        rxQueryBuilder = RxQueryBuilder(queryBuilder, rxDao)
    }

    @Test
    fun where() {
        val where = mock<Where<TestModel, Long>>()
        whenever(queryBuilder.where())
                .thenReturn(where)

        val rxWhere = rxQueryBuilder.where()

        Assertions.assertThat(rxWhere).isInstanceOf(RxQueryWhere::class.java)
        Assertions.assertThat(rxWhere.where).isEqualTo(where)
    }

    @Test
    fun prepare() {
        val preparedStatement = mock<PreparedQuery<TestModel>>()
        whenever(queryBuilder.prepare())
                .thenReturn(preparedStatement)

        rxQueryBuilder.prepare()
                .subscribe({
                    Assertions.assertThat(it).isEqualTo(preparedStatement)
                }, {
                    throw IllegalStateException("call not expected", it)
                })
    }

    @Test
    fun selectColumns_vararg() {
        val res = rxQueryBuilder.selectColumns("col1", "col2", "col3")

        verify(queryBuilder).selectColumns(eq("col1"), eq("col2"), eq("col3"))
        assertThat(res).isEqualTo(rxQueryBuilder)
    }

    @Test
    fun selectColumns_collection() {
        rxQueryBuilder.selectColumns(listOf("col1", "col2", "col3"))

        verify(queryBuilder).selectColumns(eq(listOf("col1", "col2", "col3")))
    }

    @Test
    fun selectRaw() {
        val res = rxQueryBuilder.selectRaw("raw1", "raw2", "raw3")

        verify(queryBuilder).selectRaw(eq("raw1"), eq("raw2"), eq("raw3"))
        assertThat(res).isEqualTo(rxQueryBuilder)
    }

    @Test
    fun groupBy() {
        val res = rxQueryBuilder.groupBy("col_group")

        verify(queryBuilder).groupBy(eq("col_group"))
        assertThat(res).isEqualTo(rxQueryBuilder)
    }

    @Test
    fun groupByRaw() {
        val res = rxQueryBuilder.groupByRaw("raw_group")

        verify(queryBuilder).groupByRaw(eq("raw_group"))
        assertThat(res).isEqualTo(rxQueryBuilder)
    }

    @Test
    fun orderBy() {
        val res = rxQueryBuilder.orderBy("order_col", true)

        verify(queryBuilder).orderBy(eq("order_col"), eq(true))
        assertThat(res).isEqualTo(rxQueryBuilder)
    }

    @Test
    fun orderByRaw() {
        val res = rxQueryBuilder.orderByRaw("order_raw")

        verify(queryBuilder).orderByRaw(eq("order_raw"))
        assertThat(res).isEqualTo(rxQueryBuilder)
    }

    @Test
    fun orderByRaw_withArgs() {
        val res = rxQueryBuilder.orderByRaw("order_raw", SelectArg("arg1", "val1"), SelectArg("arg2", "val2"))

        verify(queryBuilder).orderByRaw(eq("order_raw"), refEq(SelectArg("arg1", "val1")), refEq(SelectArg("arg2", "val2")))
        assertThat(res).isEqualTo(rxQueryBuilder)
    }

    @Test
    fun distinct() {
        val res = rxQueryBuilder.distinct()

        verify(queryBuilder).distinct()
        assertThat(res).isEqualTo(rxQueryBuilder)
    }

    @Test
    fun limit() {
        val res = rxQueryBuilder.limit(42L)

        verify(queryBuilder).limit(eq(42L))
        assertThat(res).isEqualTo(rxQueryBuilder)
    }

    @Test
    fun limit_noLimit() {
        val res = rxQueryBuilder.limit(null)

        verify(queryBuilder).limit(eq(null))
        assertThat(res).isEqualTo(rxQueryBuilder)
    }

    @Test
    fun offset() {
        val res = rxQueryBuilder.offset(56L)

        verify(queryBuilder).offset(eq(56L))
        assertThat(res).isEqualTo(rxQueryBuilder)
    }

    @Test
    fun offset_noOffset() {
        val res = rxQueryBuilder.offset(null)

        verify(queryBuilder).offset(eq(null))
        assertThat(res).isEqualTo(rxQueryBuilder)
    }

    @Test
    fun setCountOf_string() {
        val res = rxQueryBuilder.setCountOf("*")

        verify(queryBuilder).setCountOf(eq("*"))
        assertThat(res).isEqualTo(rxQueryBuilder)
    }

    @Test
    fun setCountOf_boolean() {
        val res = rxQueryBuilder.setCountOf(true)

        verify(queryBuilder).setCountOf(eq("*"))
        assertThat(res).isEqualTo(rxQueryBuilder)
    }

    @Test
    fun setCountOf_disable() {
        val res = rxQueryBuilder.setCountOf(false)

        verify(queryBuilder).setCountOf(eq(null))
        assertThat(res).isEqualTo(rxQueryBuilder)
    }

    @Test
    fun having() {
        val res = rxQueryBuilder.having("having")

        verify(queryBuilder).having(eq("having"))
        assertThat(res).isEqualTo(rxQueryBuilder)
    }

    @Test
    fun join_queryBuilder() {
        val secondQueryBuilder = mock<QueryBuilder<TestModel, Long>>()
        val secondRxQueryBuilder = RxQueryBuilder(secondQueryBuilder, rxDao)

        val res = rxQueryBuilder.join(secondRxQueryBuilder)

        verify(queryBuilder).join(eq(secondQueryBuilder))
        assertThat(res).isEqualTo(rxQueryBuilder)
    }

    @Test
    fun query() {
        val preparedStatement = mock<PreparedQuery<TestModel>>()
        whenever(queryBuilder.prepare())
                .thenReturn(preparedStatement)

        whenever(rxDao.query(any())).thenReturn(Flowable.empty())

        rxQueryBuilder.query()
                .subscribe({
                    throw IllegalStateException("call not expected")
                }, {
                    throw IllegalStateException("call not expected", it)
                })

        verify(rxDao).query(eq(preparedStatement))
    }

    @Test
    fun queryForFirst() {
        val preparedStatement = mock<PreparedQuery<TestModel>>()
        whenever(queryBuilder.prepare())
                .thenReturn(preparedStatement)

        whenever(rxDao.queryForFirst(any())).thenReturn(Maybe.empty())

        rxQueryBuilder.queryForFirst()
                .subscribe({
                    throw IllegalStateException("call not expected")
                }, {
                    throw IllegalStateException("call not expected", it)
                })

        verify(rxDao).queryForFirst(eq(preparedStatement))
    }

    @Test
    fun countOf() {
        val preparedStatement = mock<PreparedQuery<TestModel>>()
        whenever(queryBuilder.prepare())
                .thenReturn(preparedStatement)

        whenever(rxDao.countOf(any())).thenReturn(Single.just(5L))

        rxQueryBuilder.countOf()
                .subscribe({
                    assertThat(it).isEqualTo(5L)
                }, {
                    throw IllegalStateException("call not expected", it)
                })

        verify(queryBuilder).setCountOf(eq("*"))
        verify(rxDao).countOf(eq(preparedStatement))
    }

    @Test
    fun countOf_string() {
        val preparedStatement = mock<PreparedQuery<TestModel>>()
        whenever(queryBuilder.prepare())
                .thenReturn(preparedStatement)

        whenever(rxDao.countOf(any())).thenReturn(Single.just(5L))

        rxQueryBuilder.countOf("count")
                .subscribe({
                    assertThat(it).isEqualTo(5L)
                }, {
                    throw IllegalStateException("call not expected", it)
                })

        verify(queryBuilder).setCountOf(eq("count"))
        verify(rxDao).countOf(eq(preparedStatement))
    }

    @Test
    fun setAlias() {
        rxQueryBuilder.setAlias("alias")
        verify(queryBuilder).setAlias(eq("alias"))
    }
}
