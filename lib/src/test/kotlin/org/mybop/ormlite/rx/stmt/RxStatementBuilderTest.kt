package org.mybop.ormlite.rx.stmt

import com.j256.ormlite.stmt.PreparedStmt
import com.j256.ormlite.stmt.StatementBuilder
import com.j256.ormlite.stmt.Where
import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.eq
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import io.reactivex.Single
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.mybop.ormlite.rx.model.TestModel
import org.mybop.ormlite.rx.stmt.where.RxWhere

class RxStatementBuilderTest {

    @Test
    fun setWhere() {
        val where = mock<Where<TestModel, Long>>()
        val statementBuilder = mock<StatementBuilder<TestModel, Long>>()

        val rxStatementBuilder = StubRxStatementBuilder(statementBuilder)
        rxStatementBuilder.setWhere(StubRxWhere(where))

        verify(statementBuilder).setWhere(eq(where))
    }

    @Test
    fun prepareStatementString() {
        val statementBuilder = mock<StatementBuilder<TestModel, Long>> {
            on { prepareStatementString() } doReturn "prepared statement string"
        }

        val rxStatementBuilder = StubRxStatementBuilder(statementBuilder)

        rxStatementBuilder.prepareStatementString()
                .subscribe { statementString ->
                    assertThat(statementString).isEqualTo("prepared statement string")
                }
        verify(statementBuilder).prepareStatementString()
    }

    @Test
    fun prepareStatementInfo() {
        val statementInfo = mock<StatementBuilder.StatementInfo>()
        val statementBuilder = mock<StatementBuilder<TestModel, Long>> {
            on { prepareStatementInfo() } doReturn statementInfo
        }

        val rxStatementBuilder = StubRxStatementBuilder(statementBuilder)

        rxStatementBuilder.prepareStatementInfo()
                .subscribe { it ->
                    assertThat(it).isEqualTo(statementInfo)
                }
        verify(statementBuilder).prepareStatementInfo()
    }

    @Test
    fun reset() {
        val statementBuilder = mock<StatementBuilder<TestModel, Long>>()
        val rxStatementBuilder = StubRxStatementBuilder(statementBuilder)

        rxStatementBuilder.reset()

        verify(statementBuilder).reset()
    }

    private class StubRxWhere<T, ID>(where: Where<T, ID>) : RxWhere<T, ID, StubRxWhere<T, ID>>(where) {
        override fun prepare(): Single<out PreparedStmt<T>> {
            return Single.just(mock())
        }
    }

    private class StubRxStatementBuilder<T, ID>(statementBuilder: StatementBuilder<T, ID>) : RxStatementBuilder<T, ID, StubRxWhere<T, ID>>(statementBuilder) {
        override fun where(): StubRxWhere<T, ID> {
            return mock()
        }

        override fun prepare(): Single<out PreparedStmt<T>> {
            return Single.just(mock())
        }
    }
}
