package org.mybop.ormlite.rx.mapper

import com.j256.ormlite.support.DatabaseResults
import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.mock
import org.junit.Assert.assertArrayEquals
import org.junit.Test

class StringArrayRowMapperTest {

    @Test
    fun mapRow() {
        val expectedResults = arrayOf("test1", "test2", "test3")
        val results = mock<DatabaseResults> {
            on { columnCount } doReturn 3
            (0..2).forEach { index ->
                on { getString(index) } doReturn expectedResults[index]
            }
        }

        val mapper = StringArrayRowMapper
        assertArrayEquals(expectedResults, mapper.mapRow(results))
    }
}
