package org.mybop.ormlite.rx.stmt

import com.j256.ormlite.stmt.PreparedUpdate
import com.j256.ormlite.stmt.UpdateBuilder
import com.j256.ormlite.stmt.Where
import com.nhaarman.mockito_kotlin.*
import io.reactivex.Completable
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.mybop.ormlite.rx.dao.RxDao
import org.mybop.ormlite.rx.model.TestModel
import org.mybop.ormlite.rx.stmt.where.RxUpdateWhere

@RunWith(MockitoJUnitRunner::class)
class RxUpdateBuilderTest {

    @Mock
    private lateinit var updateBuilder: UpdateBuilder<TestModel, Long>

    @Mock
    private lateinit var rxDao: RxDao<TestModel, Long>

    private lateinit var rxUpdateBuilder: RxUpdateBuilder<TestModel, Long>

    @Before
    fun setUp() {
        rxUpdateBuilder = RxUpdateBuilder(updateBuilder, rxDao)
    }

    @Test
    fun where() {
        val where = mock<Where<TestModel, Long>>()
        whenever(updateBuilder.where())
                .thenReturn(where)

        val rxWhere = rxUpdateBuilder.where()

        assertThat(rxWhere).isInstanceOf(RxUpdateWhere::class.java)
        assertThat(rxWhere.where).isEqualTo(where)
    }

    @Test
    fun prepare() {
        val preparedStatement = mock<PreparedUpdate<TestModel>>()
        whenever(updateBuilder.prepare())
                .thenReturn(preparedStatement)

        rxUpdateBuilder.prepare()
                .subscribe({
                    assertThat(it).isEqualTo(preparedStatement)
                }, {
                    throw IllegalStateException("call not expected", it)
                })
    }

    @Test
    fun updateColumnValue() {
        rxUpdateBuilder.updateColumnValue("column", "value")

        verify(updateBuilder).updateColumnValue(eq("column"), eq("value"))
    }

    @Test
    fun updateColumnExpression() {
        rxUpdateBuilder.updateColumnExpression("column", "expression")

        verify(updateBuilder).updateColumnExpression(eq("column"), eq("expression"))
    }

    @Test
    fun escapeColumnName() {
        whenever(updateBuilder.escapeColumnName(eq("column"))).thenReturn("escaped_column")

        val res = rxUpdateBuilder.escapeColumnName("column")

        assertThat(res).isEqualTo("escaped_column")

        verify(updateBuilder).escapeColumnName(eq("column"))
    }

    @Test
    fun escapeColumnName_stringBuilder() {
        val sb = mock<StringBuilder>()

        rxUpdateBuilder.escapeColumnName(sb, "column")

        verify(updateBuilder).escapeColumnName(eq(sb), eq("column"))
    }

    @Test
    fun escapeValue() {
        whenever(updateBuilder.escapeValue(eq("value"))).thenReturn("escaped_value")

        val res = rxUpdateBuilder.escapeValue("value")

        assertThat(res).isEqualTo("escaped_value")

        verify(updateBuilder).escapeValue(eq("value"))
    }

    @Test
    fun escapeValue_stringBuilder() {
        val sb = mock<StringBuilder>()

        rxUpdateBuilder.escapeValue(sb, "value")

        verify(updateBuilder).escapeValue(eq(sb), eq("value"))
    }

    @Test
    fun update() {
        val preparedStatement = mock<PreparedUpdate<TestModel>>()
        whenever(updateBuilder.prepare())
                .thenReturn(preparedStatement)

        whenever(rxDao.update(any<PreparedUpdate<TestModel>>()))
                .thenReturn(Completable.complete())

        rxUpdateBuilder.update()
                .subscribe()

        verify(rxDao).update(eq(preparedStatement))
    }
}
