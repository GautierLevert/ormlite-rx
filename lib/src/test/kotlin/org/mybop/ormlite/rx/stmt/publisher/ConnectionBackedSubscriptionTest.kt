package org.mybop.ormlite.rx.stmt.publisher

import com.j256.ormlite.support.ConnectionSource
import com.j256.ormlite.support.DatabaseConnection
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.never
import com.nhaarman.mockito_kotlin.verify
import org.junit.Assert.assertTrue
import org.junit.Test
import org.reactivestreams.Subscriber

class ConnectionBackedSubscriptionTest {

    @Test
    fun request() {
        val abstractMethods = mock<ConnectionBackedSubscriptionAbstractMethods>()

        val subscription = TestConnectionBackedSubscription(
                mock<Subscriber<Any>>(),
                mock(),
                mock(),
                abstractMethods
        )

        subscription.request(1)
        verify(abstractMethods).doRequest(1)
    }

    @Test
    fun request_canceled() {
        val abstractMethods = mock<ConnectionBackedSubscriptionAbstractMethods>()

        val subscription = TestConnectionBackedSubscription(
                mock<Subscriber<Any>>(),
                mock(),
                mock(),
                abstractMethods
        )

        subscription.cancel()
        subscription.request(1)
        verify(abstractMethods, never()).doRequest(1)
    }

    @Test
    fun request_closed() {
        val subscriber = mock<Subscriber<Any>>()
        val abstractMethods = mock<ConnectionBackedSubscriptionAbstractMethods>()

        val subscription = TestConnectionBackedSubscription(
                subscriber,
                mock(),
                mock(),
                abstractMethods
        )

        subscription.closeConnectionQuietly()
        subscription.request(1)
        verify(abstractMethods, never()).doRequest(1)
        verify(subscriber).onError(any())
    }

    @Test
    fun doRequest_throwsException() {
        val subscriber = mock<Subscriber<Any>>()
        val connectionSource = mock<ConnectionSource>()
        val connection = mock<DatabaseConnection>()

        val exception = IllegalStateException("Test")

        val subscription = TestConnectionBackedSubscription(
                subscriber,
                connectionSource,
                connection,
                object : ConnectionBackedSubscriptionAbstractMethods {
                    override fun doRequest(n: Long) {
                        throw exception
                    }

                    override fun close() {}
                }
        )

        subscription.request(1)
        // Exception has been forwarded to subscriber
        verify(subscriber).onError(exception)
        // And connection has been closed
        assertTrue(subscription.closed)
        verify(connectionSource).releaseConnection(connection)
    }

    @Test
    fun closeConnectionQuietly() {
        val connectionSource = mock<ConnectionSource>()
        val connection = mock<DatabaseConnection>()
        val abstractMethods = mock<ConnectionBackedSubscriptionAbstractMethods>()

        val subscription = TestConnectionBackedSubscription(
                mock<Subscriber<Any>>(),
                connectionSource,
                connection,
                abstractMethods
        )

        subscription.closeConnectionQuietly()
        assertTrue(subscription.closed)
        verify(connectionSource).releaseConnection(connection)
        verify(abstractMethods).close()

        subscription.closeConnectionQuietly()
        assertTrue(subscription.closed)
        verify(connectionSource).releaseConnection(connection)
        verify(abstractMethods).close()
    }

    @Test
    fun cancel() {
        val connectionSource = mock<ConnectionSource>()
        val connection = mock<DatabaseConnection>()
        val abstractMethods = mock<ConnectionBackedSubscriptionAbstractMethods>()

        val subscription = TestConnectionBackedSubscription(
                mock<Subscriber<Any>>(),
                connectionSource,
                connection,
                abstractMethods
        )

        subscription.cancel()
        assertTrue(subscription.canceled)
        assertTrue(subscription.closed)
        verify(connectionSource).releaseConnection(connection)
        verify(abstractMethods).close()
    }

    private class TestConnectionBackedSubscription<T>(
            subscriber: Subscriber<T>,
            connectionSource: ConnectionSource,
            connection: DatabaseConnection,
            val abstractMethods: ConnectionBackedSubscriptionAbstractMethods
    ) : ConnectionBackedSubscription<T>(subscriber, connectionSource, connection) {
        override fun doRequest(n: Long) = abstractMethods.doRequest(n)
        override fun close() = abstractMethods.close()
    }

    private interface ConnectionBackedSubscriptionAbstractMethods {
        fun doRequest(n: Long)
        fun close()
    }
}
