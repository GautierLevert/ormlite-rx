package org.mybop.ormlite.rx.mapper

import com.j256.ormlite.support.DatabaseResults
import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.mock
import org.junit.Assert.assertEquals
import org.junit.Test

class LongRowMapperTest {

    @Test
    fun mapRow() {
        val results = mock<DatabaseResults> {
            on { it.getLong(0) } doReturn 42L
        }

        val mapper = LongRowMapper
        assertEquals(42L, mapper.mapRow(results))
    }
}
