package org.mybop.ormlite.rx.model

import com.j256.ormlite.field.DatabaseField
import com.j256.ormlite.table.DatabaseTable

@DatabaseTable(tableName = "test")
class TestModel() {

    constructor(id: Long) : this() {
        this.id = id
    }

    @DatabaseField(id = true)
    var id: Long? = null

    @DatabaseField
    var value: Long = 0

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is TestModel) return false

        if (id != other.id) return false
        if (value != other.value) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id?.hashCode() ?: 0
        result = 31 * result + value.hashCode()
        return result
    }
}
