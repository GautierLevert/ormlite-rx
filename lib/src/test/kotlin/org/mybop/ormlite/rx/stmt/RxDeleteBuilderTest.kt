package org.mybop.ormlite.rx.stmt

import com.j256.ormlite.stmt.DeleteBuilder
import com.j256.ormlite.stmt.PreparedDelete
import com.j256.ormlite.stmt.Where
import com.nhaarman.mockito_kotlin.*
import io.reactivex.Completable
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.mybop.ormlite.rx.dao.RxDao
import org.mybop.ormlite.rx.model.TestModel
import org.mybop.ormlite.rx.stmt.where.RxDeleteWhere

@RunWith(MockitoJUnitRunner::class)
class RxDeleteBuilderTest {

    @Mock
    private lateinit var deleteBuilder: DeleteBuilder<TestModel, Long>

    @Mock
    private lateinit var rxDao: RxDao<TestModel, Long>

    private lateinit var rxDeleteBuilder: RxDeleteBuilder<TestModel, Long>

    @Before
    fun setUp() {
        rxDeleteBuilder = RxDeleteBuilder(deleteBuilder, rxDao)
    }

    @Test
    fun where() {
        val where = mock<Where<TestModel, Long>>()
        whenever(deleteBuilder.where())
                .thenReturn(where)

        val rxWhere = rxDeleteBuilder.where()

        assertThat(rxWhere).isInstanceOf(RxDeleteWhere::class.java)
        assertThat(rxWhere.where).isEqualTo(where)
    }

    @Test
    fun prepare() {
        val preparedStatement = mock<PreparedDelete<TestModel>>()
        whenever(deleteBuilder.prepare())
                .thenReturn(preparedStatement)

        rxDeleteBuilder.prepare()
                .subscribe({
                    assertThat(it).isEqualTo(preparedStatement)
                }, {
                    throw IllegalStateException("call not expected", it)
                })
    }

    @Test
    fun delete() {
        val preparedStatement = mock<PreparedDelete<TestModel>>()
        whenever(deleteBuilder.prepare())
                .thenReturn(preparedStatement)

        whenever(rxDao.delete(any<PreparedDelete<TestModel>>()))
                .thenReturn(Completable.complete())

        rxDeleteBuilder.delete()
                .subscribe()

        verify(rxDao).delete(eq(preparedStatement))
    }

}
