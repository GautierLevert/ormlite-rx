package org.mybop.ormlite.rx.dao

import com.j256.ormlite.dao.BaseDaoImpl
import com.j256.ormlite.stmt.GenericRowMapper
import com.j256.ormlite.stmt.PreparedDelete
import com.j256.ormlite.stmt.PreparedQuery
import com.j256.ormlite.stmt.PreparedUpdate
import com.j256.ormlite.support.ConnectionSource
import com.j256.ormlite.table.TableInfo
import com.nhaarman.mockito_kotlin.*
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Maybe
import io.reactivex.Single
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.mybop.ormlite.rx.misc.BatchCompletable
import org.mybop.ormlite.rx.stmt.CommonlyUsedStatements
import org.mybop.ormlite.rx.stmt.RxRawStatementExecutor
import org.mybop.ormlite.rx.stmt.RxStatementExecutor

class BaseRxDaoImplTest {

    @Test
    fun queryForId() {
        val expectedResult = Any()
        val queryForId = mock<PreparedQuery<Any>>()
        val commonlyUsedStatements = mock<CommonlyUsedStatements<Any, Any>> {
            on { it.queryForId(any()) } doReturn Single.just(queryForId)
        }
        val statementExecutor = mock<RxStatementExecutor<Any, Any>> {
            on { it.queryForFirst(any(), eq(queryForId), anyOrNull()) } doReturn Maybe.just(expectedResult)
        }
        val dao = mock<BaseDaoImpl<Any, Any>> {
            on { connectionSource } doReturn mock<ConnectionSource>()
        }
        val rxDao = BaseRxDaoImpl(
                statementExecutor,
                mock(),
                commonlyUsedStatements,
                dao
        )

        val result = rxDao.queryForId(Any()).blockingGet()
        assertThat(result).isEqualTo(expectedResult)
    }

    @Test
    fun queryForFirst() {
        val expectedResult = Any()
        val query = mock<PreparedQuery<Any>>()
        val statementExecutor = mock<RxStatementExecutor<Any, Any>> {
            on { it.queryForFirst(any(), eq(query), anyOrNull()) } doReturn Maybe.just(expectedResult)
        }
        val dao = mock<BaseDaoImpl<Any, Any>> {
            on { connectionSource } doReturn mock<ConnectionSource>()
        }
        val rxDao = BaseRxDaoImpl(
                statementExecutor,
                mock(),
                mock(),
                dao
        )

        val result = rxDao.queryForFirst(query).blockingGet()
        assertThat(result).isEqualTo(expectedResult)
    }

    @Test
    fun queryForAll() {
        val expectedResults = listOf(Any())
        val queryForAll = mock<PreparedQuery<Any>>()
        val commonlyUsedStatements = mock<CommonlyUsedStatements<Any, Any>> {
            on { it.queryForAll } doReturn Single.just(queryForAll)
        }
        val statementExecutor = mock<RxStatementExecutor<Any, Any>> {
            on { it.query(any(), eq(queryForAll), anyOrNull()) } doReturn Flowable.fromIterable(expectedResults)
        }
        val dao = mock<BaseDaoImpl<Any, Any>> {
            on { connectionSource } doReturn mock<ConnectionSource>()
        }
        val rxDao = BaseRxDaoImpl(
                statementExecutor,
                mock(),
                commonlyUsedStatements,
                dao
        )

        val results = rxDao.queryForAll().blockingIterable().toList()
        assertThat(results).isEqualTo(expectedResults)
    }

    @Test
    fun query() {
        val expectedResults = listOf(Any())
        val query = mock<PreparedQuery<Any>>()
        val statementExecutor = mock<RxStatementExecutor<Any, Any>> {
            on { it.query(any(), eq(query), anyOrNull()) } doReturn Flowable.fromIterable(expectedResults)
        }
        val dao = mock<BaseDaoImpl<Any, Any>> {
            on { connectionSource } doReturn mock<ConnectionSource>()
        }
        val rxDao = BaseRxDaoImpl(
                statementExecutor,
                mock(),
                mock(),
                dao
        )

        val results = rxDao.query(query).blockingIterable().toList()
        assertThat(results).isEqualTo(expectedResults)
    }

    @Test
    fun create() {
        val data = Any()
        val statementExecutor = mock<RxStatementExecutor<Any, Any>> {
            on { it.create(any(), eq(data), anyOrNull()) } doReturn Completable.complete()
        }
        val dao = mock<BaseDaoImpl<Any, Any>> {
            on { connectionSource } doReturn mock<ConnectionSource>()
        }
        val rxDao = BaseRxDaoImpl(
                statementExecutor,
                mock(),
                mock(),
                dao
        )

        val result = rxDao.create(data).blockingGet()
        assertThat(result).isEqualTo(data)
        verify(statementExecutor).create(any(), eq(data), anyOrNull())
    }

    @Test
    fun create_collection() {
        val data: Collection<Int> = listOf(1, 2, 3)
        val dao = mock<BaseDaoImpl<Int, Any>> {
            on { create(data) } doReturn data.size
            on { connectionSource } doReturn mock<ConnectionSource>()
            on { tableInfo } doReturn mock<TableInfo<Int, Any>>()
        }
        val rxDao = BaseRxDaoImpl(
                mock(),
                mock(),
                mock(),
                dao
        )

        val result = rxDao.create(data)
        assertThat(result).isInstanceOf(BatchCompletable::class.java)
    }

    @Test
    fun createIfNotExists_create() {
        val data = Any()
        val statementExecutor = mock<RxStatementExecutor<Any, Any>> {
            on { it.create(any(), eq(data), anyOrNull()) } doReturn Completable.complete()
        }
        val dao = mock<BaseDaoImpl<Any, Any>> {
            on { connectionSource } doReturn mock<ConnectionSource>()
        }
        val rxDao = BaseRxDaoImpl(
                statementExecutor,
                mock(),
                mock(),
                dao
        )

        val result = rxDao.createIfNotExists(data).blockingGet()
        assertThat(result).isEqualTo(data)
        verify(statementExecutor).create(any(), eq(data), anyOrNull())
    }

    @Test
    fun createIfNotExists_refresh() {
        val data = Any()
        val refreshedData = Any()
        val statementExecutor = mock<RxStatementExecutor<Any, Any>> {
            on { it.create(any(), eq(data), anyOrNull()) } doReturn Completable.error(Exception())
            on { it.refresh(any(), eq(data), anyOrNull()) } doReturn Single.just(refreshedData)
        }
        val dao = mock<BaseDaoImpl<Any, Any>> {
            on { connectionSource } doReturn mock<ConnectionSource>()
        }
        val rxDao = BaseRxDaoImpl(
                statementExecutor,
                mock(),
                mock(),
                dao
        )

        val result = rxDao.createIfNotExists(data).blockingGet()
        assertThat(result).isEqualTo(refreshedData)
    }

    @Test
    fun createOrUpdate_create() {
        val data = Any()
        val statementExecutor = mock<RxStatementExecutor<Any, Any>> {
            on { it.create(any(), eq(data), anyOrNull()) } doReturn Completable.complete()
        }
        val dao = mock<BaseDaoImpl<Any, Any>> {
            on { connectionSource } doReturn mock<ConnectionSource>()
        }
        val rxDao = BaseRxDaoImpl(
                statementExecutor,
                mock(),
                mock(),
                dao
        )

        val result = rxDao.createOrUpdate(data).blockingGet()
        assertThat(result).isEqualTo(data)
        verify(statementExecutor).create(any(), eq(data), anyOrNull())
        verify(statementExecutor, never()).update(any(), eq(data), anyOrNull())
    }

    @Test
    fun createOrUpdate_update() {
        val data = Any()
        val statementExecutor = mock<RxStatementExecutor<Any, Any>> {
            on { it.create(any(), eq(data), anyOrNull()) } doReturn Completable.error(Exception())
            on { it.update(any(), eq(data), anyOrNull()) } doReturn Completable.complete()
        }
        val dao = mock<BaseDaoImpl<Any, Any>> {
            on { connectionSource } doReturn mock<ConnectionSource>()
        }
        val rxDao = BaseRxDaoImpl(
                statementExecutor,
                mock(),
                mock(),
                dao
        )

        val result = rxDao.createOrUpdate(data).blockingGet()
        assertThat(result).isEqualTo(data)
        verify(statementExecutor).create(any(), eq(data), anyOrNull())
        verify(statementExecutor).update(any(), eq(data), anyOrNull())
    }

    @Test
    fun update() {
        val data = Any()
        val statementExecutor = mock<RxStatementExecutor<Any, Any>> {
            on { it.update(any(), eq(data), anyOrNull()) } doReturn Completable.complete()
        }
        val dao = mock<BaseDaoImpl<Any, Any>> {
            on { connectionSource } doReturn mock<ConnectionSource>()
        }
        val rxDao = BaseRxDaoImpl(
                statementExecutor,
                mock(),
                mock(),
                dao
        )

        val result = rxDao.update(data).blockingGet()
        assertThat(result).isEqualTo(data)
        verify(statementExecutor).update(any(), eq(data), anyOrNull())
    }

    @Test
    fun updateId() {
        val data = Any()
        val newId = Any()
        val statementExecutor = mock<RxStatementExecutor<Any, Any>> {
            on { it.updateId(any(), eq(data), eq(newId), anyOrNull()) } doReturn Completable.complete()
        }
        val dao = mock<BaseDaoImpl<Any, Any>> {
            on { connectionSource } doReturn mock<ConnectionSource>()
        }
        val rxDao = BaseRxDaoImpl(
                statementExecutor,
                mock(),
                mock(),
                dao
        )

        val result = rxDao.updateId(data, newId).blockingGet()
        assertThat(result).isEqualTo(data)
        verify(statementExecutor).updateId(any(), eq(data), eq(newId), anyOrNull())
    }

    @Test
    fun update_prepared() {
        val update = mock<PreparedUpdate<Any>>()
        val statementExecutor = mock<RxStatementExecutor<Any, Any>> {
            on { it.update(any(), eq(update)) } doReturn Single.just(42)
        }
        val dao = mock<BaseDaoImpl<Any, Any>> {
            on { connectionSource } doReturn mock<ConnectionSource>()
        }
        val rxDao = BaseRxDaoImpl(
                statementExecutor,
                mock(),
                mock(),
                dao
        )

        rxDao.update(preparedUpdate = update).blockingAwait()
        verify(statementExecutor).update(any(), eq(update))
    }

    @Test
    fun refresh() {
        val data = Any()
        val refreshedData = Any()
        val statementExecutor = mock<RxStatementExecutor<Any, Any>> {
            on { it.refresh(any(), eq(data), anyOrNull()) } doReturn Single.just(refreshedData)
        }
        val dao = mock<BaseDaoImpl<Any, Any>> {
            on { connectionSource } doReturn mock<ConnectionSource>()
        }
        val rxDao = BaseRxDaoImpl(
                statementExecutor,
                mock(),
                mock(),
                dao
        )

        val result = rxDao.refresh(data).blockingGet()
        assertThat(result).isEqualTo(refreshedData)
        verify(statementExecutor).refresh(any(), eq(data), anyOrNull())
    }

    @Test
    fun delete() {
        val data = Any()
        val statementExecutor = mock<RxStatementExecutor<Any, Any>> {
            on { it.delete(any(), eq(data), anyOrNull()) } doReturn Completable.complete()
        }
        val dao = mock<BaseDaoImpl<Any, Any>> {
            on { connectionSource } doReturn mock<ConnectionSource>()
        }
        val rxDao = BaseRxDaoImpl(
                statementExecutor,
                mock(),
                mock(),
                dao
        )

        rxDao.delete(data).blockingAwait()
        verify(statementExecutor).delete(any(), eq(data), anyOrNull())
    }

    @Test
    fun deleteById() {
        val id = Any()
        val statementExecutor = mock<RxStatementExecutor<Any, Any>> {
            on { it.deleteById(any(), eq(id), anyOrNull()) } doReturn Completable.complete()
        }
        val dao = mock<BaseDaoImpl<Any, Any>> {
            on { connectionSource } doReturn mock<ConnectionSource>()
        }
        val rxDao = BaseRxDaoImpl(
                statementExecutor,
                mock(),
                mock(),
                dao
        )

        rxDao.deleteById(id).blockingAwait()
        verify(statementExecutor).deleteById(any(), eq(id), anyOrNull())
    }

    @Test
    fun deleteObjects() {
        val data = listOf(Any())
        val statementExecutor = mock<RxStatementExecutor<Any, Any>> {
            on { it.deleteObjects(any(), eq(data), anyOrNull()) } doReturn Completable.complete()
        }
        val dao = mock<BaseDaoImpl<Any, Any>> {
            on { connectionSource } doReturn mock<ConnectionSource>()
        }
        val rxDao = BaseRxDaoImpl(
                statementExecutor,
                mock(),
                mock(),
                dao
        )

        rxDao.delete(dataCollection = data).blockingAwait()
        verify(statementExecutor).deleteObjects(any(), eq(data), anyOrNull())
    }

    @Test
    fun deleteByIds() {
        val ids = listOf(Any())
        val statementExecutor = mock<RxStatementExecutor<Any, Any>> {
            on { it.deleteIds(any(), eq(ids), anyOrNull()) } doReturn Completable.complete()
        }
        val dao = mock<BaseDaoImpl<Any, Any>> {
            on { connectionSource } doReturn mock<ConnectionSource>()
        }
        val rxDao = BaseRxDaoImpl(
                statementExecutor,
                mock(),
                mock(),
                dao
        )

        rxDao.deleteIds(ids).blockingAwait()
        verify(statementExecutor).deleteIds(any(), eq(ids), anyOrNull())
    }

    @Test
    fun delete_prepared() {
        val delete = mock<PreparedDelete<Any>>()
        val statementExecutor = mock<RxStatementExecutor<Any, Any>> {
            on { it.delete(any(), eq(delete)) } doReturn Single.just(42)
        }
        val dao = mock<BaseDaoImpl<Any, Any>> {
            on { connectionSource } doReturn mock<ConnectionSource>()
        }
        val rxDao = BaseRxDaoImpl(
                statementExecutor,
                mock(),
                mock(),
                dao
        )

        rxDao.delete(preparedDelete = delete).blockingAwait()
        verify(statementExecutor).delete(any(), eq(delete))
    }

    @Test
    fun countOf() {
        val expectedResult = 42L
        val countAll = mock<PreparedQuery<Any>>()
        val commonlyUsedStatements = mock<CommonlyUsedStatements<Any, Any>> {
            on { it.countAll } doReturn Single.just(countAll)
        }
        val statementExecutor = mock<RxStatementExecutor<Any, Any>> {
            on { it.queryForCount(any(), eq(countAll)) } doReturn Single.just(expectedResult)
        }
        val dao = mock<BaseDaoImpl<Any, Any>> {
            on { connectionSource } doReturn mock<ConnectionSource>()
        }
        val rxDao = BaseRxDaoImpl(
                statementExecutor,
                mock(),
                commonlyUsedStatements,
                dao
        )

        val result = rxDao.countOf().blockingGet()
        assertThat(result).isEqualTo(expectedResult)
    }

    @Test
    fun countOf_prepared() {
        val expectedResult = 42L
        val query = mock<PreparedQuery<Any>>()
        val statementExecutor = mock<RxStatementExecutor<Any, Any>> {
            on { it.queryForCount(any(), eq(query)) } doReturn Single.just(expectedResult)
        }
        val dao = mock<BaseDaoImpl<Any, Any>> {
            on { connectionSource } doReturn mock<ConnectionSource>()
        }
        val rxDao = BaseRxDaoImpl(
                statementExecutor,
                mock(),
                mock(),
                dao
        )

        val result = rxDao.countOf(query).blockingGet()
        assertThat(result).isEqualTo(expectedResult)
    }

    @Test
    fun queryRaw() {
        val expectedResults = listOf(Any())
        val rawStatementExecutor = mock<RxRawStatementExecutor<Any, Any>> {
            on { queryRaw<Any>(any(), any(), any(), any(), anyOrNull()) } doReturn Flowable.fromIterable(expectedResults)
        }
        val dao = mock<BaseDaoImpl<Any, Any>> {
            on { connectionSource } doReturn mock<ConnectionSource>()
        }
        val rxDao = BaseRxDaoImpl(
                mock(),
                rawStatementExecutor,
                mock(),
                dao
        )

        val results = rxDao.queryRaw("SELECT * FROM test WHERE id = ?", mock<GenericRowMapper<Any>>(), "test")

        assertThat(results.blockingIterable().toList()).containsExactlyElementsOf(expectedResults)
    }

    @Test
    fun queryRawFirst() {
        val expectedResult = Any()
        val rawStatementExecutor = mock<RxRawStatementExecutor<Any, Any>> {
            on { queryRawFirst<Any>(any(), any(), any(), any(), anyOrNull()) } doReturn Maybe.just(expectedResult)
        }
        val dao = mock<BaseDaoImpl<Any, Any>> {
            on { connectionSource } doReturn mock<ConnectionSource>()
        }
        val rxDao = BaseRxDaoImpl(
                mock(),
                rawStatementExecutor,
                mock(),
                dao
        )

        val results = rxDao.queryRawFirst("SELECT * FROM test WHERE id = ?", mock<GenericRowMapper<Any>>(), "test")

        assertThat(results.blockingGet()).isEqualTo(expectedResult)
    }

    @Test
    fun executeRaw() {
        val rawStatementExecutor = mock<RxRawStatementExecutor<Any, Any>> {
            on { executeRaw(any(), any(), anyArray()) } doReturn Completable.complete()
        }
        val dao = mock<BaseDaoImpl<Any, Any>> {
            on { connectionSource } doReturn mock<ConnectionSource>()
        }
        val rxDao = BaseRxDaoImpl(
                mock(),
                rawStatementExecutor,
                mock(),
                dao
        )

        rxDao.executeRaw("UPDATE test SET value = ? WHERE id = ?", "value", "testId")
                .blockingAwait()
    }

    @Test
    fun updateRaw() {
        val rawStatementExecutor = mock<RxRawStatementExecutor<Any, Any>> {
            on { updateRaw(any(), any(), anyArray()) } doReturn Completable.complete()
        }
        val dao = mock<BaseDaoImpl<Any, Any>> {
            on { connectionSource } doReturn mock<ConnectionSource>()
        }
        val rxDao = BaseRxDaoImpl(
                mock(),
                rawStatementExecutor,
                mock(),
                dao
        )

        rxDao.updateRaw("UPDATE test SET value = ? WHERE id = ?", "value", "testId")
                .blockingAwait()
    }
}
