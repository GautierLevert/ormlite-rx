package org.mybop.ormlite.rx.mapper

import com.j256.ormlite.support.DatabaseResults
import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.mock
import org.junit.Assert.assertEquals
import org.junit.Test

class DoubleRowMapperTest {

    @Test
    fun mapRow() {
        val results = mock<DatabaseResults> {
            on { it.getDouble(0) } doReturn 42.0
        }

        val mapper = DoubleRowMapper
        assertEquals(42.0, mapper.mapRow(results), 0.001)
    }
}
