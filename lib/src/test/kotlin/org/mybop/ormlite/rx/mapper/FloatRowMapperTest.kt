package org.mybop.ormlite.rx.mapper

import com.j256.ormlite.support.DatabaseResults
import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.mock
import org.junit.Assert.assertEquals
import org.junit.Test

class FloatRowMapperTest {

    @Test
    fun mapRow() {
        val results = mock<DatabaseResults> {
            on { it.getFloat(0) } doReturn 42.0F
        }

        val mapper = FloatRowMapper
        assertEquals(42.0F, mapper.mapRow(results), 0.001F)
    }
}
