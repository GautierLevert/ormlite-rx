package org.mybop.ormlite.rx.stmt.where

import com.j256.ormlite.stmt.PreparedQuery
import com.j256.ormlite.stmt.Where
import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.mock
import io.reactivex.Flowable
import io.reactivex.Maybe
import io.reactivex.Single
import org.assertj.core.api.Assertions
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.mybop.ormlite.rx.model.TestModel
import org.mybop.ormlite.rx.stmt.RxQueryBuilder

class RxQueryWhereTest {

    @Test
    fun prepare() {
        val where = mock<Where<TestModel, Long>>()

        val preparedStatement = mock<PreparedQuery<TestModel>>()
        val rxStatementBuilder = mock<RxQueryBuilder<TestModel, Long>> {
            on { prepare() } doReturn Single.just(preparedStatement)
        }

        val rxWhere = RxQueryWhere(where, rxStatementBuilder)

        rxWhere.prepare().subscribe { rxPreparedStatement ->
            Assertions.assertThat(rxPreparedStatement).isEqualTo(preparedStatement)
        }
    }

    @Test
    fun query() {
        val where = mock<Where<TestModel, Long>>()

        val rxStatementBuilder = mock<RxQueryBuilder<TestModel, Long>> {
            on { query() } doReturn Flowable.just(TestModel(1), TestModel(2))
        }

        val rxWhere = RxQueryWhere(where, rxStatementBuilder)

        val results = rxWhere.query()
        results.count().subscribe { count ->
            assertThat(count).isEqualTo(2)
        }
        results.firstElement().subscribe { first ->
            assertThat(first.id).isEqualTo(1)
        }
        results.lastElement().subscribe { first ->
            assertThat(first.id).isEqualTo(2)
        }
    }

    @Test
    fun queryRaw() {
        val where = mock<Where<TestModel, Long>>()

        val rxStatementBuilder = mock<RxQueryBuilder<TestModel, Long>> {
            on { queryRaw() } doReturn Flowable.just(arrayOf("s11", "s12"), arrayOf("s21", "s22"))
        }

        val rxWhere = RxQueryWhere(where, rxStatementBuilder)

        assertThat(rxWhere.queryRaw().blockingIterable().toList()).containsExactlyElementsOf(listOf(arrayOf("s11", "s12"), arrayOf("s21", "s22")))
    }

    @Test
    fun queryForFirst() {
        val where = mock<Where<TestModel, Long>>()

        val rxStatementBuilder = mock<RxQueryBuilder<TestModel, Long>> {
            on { queryForFirst() } doReturn Maybe.just(TestModel(4))
        }

        val rxWhere = RxQueryWhere(where, rxStatementBuilder)

        rxWhere.queryForFirst().subscribe { foo ->
            assertThat(foo.id).isEqualTo(4)
        }
    }

    @Test
    fun queryRawFirst() {
        val where = mock<Where<TestModel, Long>>()

        val rxStatementBuilder = mock<RxQueryBuilder<TestModel, Long>> {
            on { queryRawFirst() } doReturn Maybe.just(arrayOf("one", "two"))
        }

        val rxWhere = RxQueryWhere(where, rxStatementBuilder)

        assertThat(rxWhere.queryRawFirst().blockingGet()).isEqualTo(arrayOf("one", "two"))
    }

    @Test
    fun countOf() {
        val where = mock<Where<TestModel, Long>>()

        val rxStatementBuilder = mock<RxQueryBuilder<TestModel, Long>> {
            on { countOf() } doReturn Single.just(12L)
        }

        val rxWhere = RxQueryWhere(where, rxStatementBuilder)

        rxWhere.countOf().subscribe { count ->
            assertThat(count).isEqualTo(12)
        }
    }
}
