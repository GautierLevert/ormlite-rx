package org.mybop.ormlite.rx.stmt.where

import com.j256.ormlite.stmt.PreparedUpdate
import com.j256.ormlite.stmt.Where
import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.mock
import io.reactivex.Completable
import io.reactivex.Single
import org.assertj.core.api.Assertions
import org.junit.Test
import org.mybop.ormlite.rx.model.TestModel
import org.mybop.ormlite.rx.stmt.RxUpdateBuilder

class RxUpdateWhereTest {

    @Test
    fun prepare() {
        val where = mock<Where<TestModel, Long>>()

        val preparedStatement = mock<PreparedUpdate<TestModel>>()
        val rxStatementBuilder = mock<RxUpdateBuilder<TestModel, Long>> {
            on { prepare() } doReturn Single.just(preparedStatement)
        }

        val rxWhere = RxUpdateWhere(where, rxStatementBuilder)

        rxWhere.prepare().subscribe { rxPreparedStatement ->
            Assertions.assertThat(rxPreparedStatement).isEqualTo(preparedStatement)
        }
    }

    @Test
    fun update() {
        val where = mock<Where<TestModel, Long>>()

        val rxStatementBuilder = mock<RxUpdateBuilder<TestModel, Long>> {
            on { update() } doReturn Completable.complete()
        }

        val rxWhere = RxUpdateWhere(where, rxStatementBuilder)

        var completed = false
        rxWhere.update().subscribe {
            completed = true
        }

        Assertions.assertThat(completed).isTrue()
    }
}
