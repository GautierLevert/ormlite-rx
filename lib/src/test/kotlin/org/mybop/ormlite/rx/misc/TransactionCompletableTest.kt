package org.mybop.ormlite.rx.misc

import com.j256.ormlite.db.DatabaseType
import com.j256.ormlite.support.ConnectionSource
import com.j256.ormlite.support.DatabaseConnection
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.inOrder
import com.nhaarman.mockito_kotlin.mock
import io.reactivex.Completable
import org.junit.Assert.assertEquals
import org.junit.Test
import java.sql.Savepoint

class TransactionCompletableTest {

    @Test
    fun subscribeActual_complete() {
        val tableName = "test"
        val savepointName = "ORMLITE1"
        val databaseType = mock<DatabaseType> {
            on { it.isNestedSavePointsSupported } doReturn true
        }
        val savepoint = mock<Savepoint> {
            on { it.savepointName } doReturn savepointName
        }
        val connection = mock<DatabaseConnection> {
            on { it.isAutoCommit } doReturn true
            on { it.isAutoCommitSupported } doReturn true
            on { it.setSavePoint(any()) } doReturn savepoint
        }
        val connectionSource = mock<ConnectionSource> {
            on { it.databaseType } doReturn databaseType
            on { it.getReadWriteConnection(tableName) } doReturn connection
            on { it.saveSpecialConnection(connection) } doReturn true
        }

        val completable = Completable.fromCallable {
            val conn = connectionSource.getReadWriteConnection(tableName)
            // Do some stuff with the connection
            connectionSource.releaseConnection(conn)
        }

        TransactionCompletable(connectionSource, tableName, completable).blockingAwait()

        inOrder(connectionSource, connection) {
            // TransactionCompletable - onSubscribe
            verify(connectionSource).getReadWriteConnection(tableName)
            verify(connectionSource).saveSpecialConnection(connection)
            verify(connection).isAutoCommit = false
            verify(connection).setSavePoint(any())

            // Completable
            verify(connectionSource).getReadWriteConnection(tableName)
            verify(connectionSource).releaseConnection(connection)

            // TransactionCompletable - onComplete
            verify(connection).commit(savepoint)

            // TransactionCompletable - onFinally
            verify(connection).isAutoCommit = true
            verify(connectionSource).clearSpecialConnection(connection)
            verify(connectionSource).releaseConnection(connection)
        }
    }

    @Test
    fun subscribeActual_error() {
        val tableName = "test"
        val savepointName = "ORMLITE1"
        val databaseType = mock<DatabaseType> {
            on { it.isNestedSavePointsSupported } doReturn true
        }
        val savepoint = mock<Savepoint> {
            on { it.savepointName } doReturn savepointName
        }
        val connection = mock<DatabaseConnection> {
            on { it.isAutoCommit } doReturn true
            on { it.isAutoCommitSupported } doReturn true
            on { it.setSavePoint(any()) } doReturn savepoint
        }
        val connectionSource = mock<ConnectionSource> {
            on { it.databaseType } doReturn databaseType
            on { it.getReadWriteConnection(tableName) } doReturn connection
            on { it.saveSpecialConnection(connection) } doReturn true
        }

        val completable = Completable.error(Exception())

        val transaction = TransactionCompletable(connectionSource, tableName, completable).test()
        assertEquals(transaction.errorCount(), 1)

        inOrder(connectionSource, connection) {
            // TransactionCompletable - onSubscribe
            verify(connectionSource).getReadWriteConnection(tableName)
            verify(connectionSource).saveSpecialConnection(connection)
            verify(connection).setSavePoint(any())

            // TransactionCompletable - onError
            verify(connection).rollback(savepoint)

            // TransactionCompletable - onFinally
            verify(connectionSource).clearSpecialConnection(connection)
            verify(connectionSource).releaseConnection(connection)
        }
    }
}
