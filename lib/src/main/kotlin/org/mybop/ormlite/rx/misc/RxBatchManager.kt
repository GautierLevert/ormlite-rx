package org.mybop.ormlite.rx.misc

import com.j256.ormlite.support.ConnectionSource
import io.reactivex.Completable

/**
 * Provides basic batch support for a [ConnectionSource].
 *
 * A batch disable auto-commit and use the same connection for every operation.
 *
 * **NOTE:** A batch is not a transaction.
 */
class RxBatchManager(private val connectionSource: ConnectionSource, private val tableName: String? = null) {

    /**
     * Wrap the [Completable] inside of a batch.
     * When the [Completable] complete or throws an exception the auto_commit is re-enabled (if enabled at the beginning).
     *
     * **WARNING:** it is up to you to properly manage threads and schedulers of your [Completable]. There could be side
     * effects of using multiple threads. We recommend to use only one thread for all the completable execution.
     *
     * @param completable
     *            Completable to be wrapped inside of the batch.
     * @return Another completable that will disable auto-commit and re-enable it at the end.
     */
    fun callInBatch(completable: Completable): Completable {
        return BatchCompletable(connectionSource, tableName, completable)
    }
}
