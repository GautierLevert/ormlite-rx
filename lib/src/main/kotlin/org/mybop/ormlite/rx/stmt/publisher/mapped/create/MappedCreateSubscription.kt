package org.mybop.ormlite.rx.stmt.publisher.mapped.create

import com.j256.ormlite.dao.ObjectCache
import com.j256.ormlite.stmt.mapped.MappedCreate
import com.j256.ormlite.support.ConnectionSource
import com.j256.ormlite.support.DatabaseConnection
import org.mybop.ormlite.rx.stmt.publisher.mapped.MappedStatementSubscription
import org.reactivestreams.Subscriber

/**
 * A subscription that will execute the insert and emit the number of rows inserted to
 * the publisher.
 *
 * The ID field of the data parameter will be automatically set if it is marked auto generated.
 *
 * @param T The class that the code will be operating on.
 * @param ID The class of the ID column associated with the class.
 * @param subscriber [Subscriber] to emit the number of rows to.
 * @param connectionSource [ConnectionSource] that have provided the connection.
 * @param connection [DatabaseConnection] on which the mapped statement will be executed.
 * @param mappedStatement Create statement that will be executed when a publisher subscribes and requests.
 * @param data Data to insert in database.
 * @param objectCache Optional [ObjectCache].
 *
 * @see MappedStatementSubscription
 */
class MappedCreateSubscription<T, ID>(
        subscriber: Subscriber<in Int>,
        private val connectionSource: ConnectionSource,
        connection: DatabaseConnection,
        mappedStatement: MappedCreate<T, ID>,
        private val data: T,
        objectCache: ObjectCache?
) : MappedStatementSubscription<T, ID, Int, MappedCreate<T, ID>>(
        subscriber, connectionSource, connection, mappedStatement, objectCache
) {

    override fun execute(
            mappedStatement: MappedCreate<T, ID>,
            connection: DatabaseConnection,
            objectCache: ObjectCache?): Int
            = mappedStatement.insert(connectionSource.databaseType, connection, data, objectCache)
}
