package org.mybop.ormlite.rx.stmt

import com.j256.ormlite.stmt.PreparedStmt
import com.j256.ormlite.stmt.StatementBuilder
import com.j256.ormlite.stmt.Where
import io.reactivex.Single
import org.mybop.ormlite.rx.stmt.where.RxWhere

/**
 * Assists in building of SQL statements for a particular table in a particular database.
 *
 * @param <T>
 *            The class that the code will be operating on.
 * @param <ID>
 *            The class of the ID column associated with the class. The T class does not require an ID field. The class
 *            needs an ID parameter however so you can use Void or Object to satisfy the compiler.
 * @see StatementBuilder
 */
abstract class RxStatementBuilder<T, ID, W : RxWhere<T, ID, *>>(
        private val statementBuilder: StatementBuilder<T, ID>
) {

    /**
     * Get the prepared statement of the current builder state
     */
    abstract fun prepare(): Single<out PreparedStmt<T>>

    /**
     * Creates and returns a new [Where][com.j256.ormlite.stmt.Where] object for this QueryBuilder that can be used to add WHERE clauses to the
     * SQL statement. Only one [Where][com.j256.ormlite.stmt.Where] object can be associated with a QueryBuilder at a time and calling this
     * method again creates a new [Where][com.j256.ormlite.stmt.Where] object and resets the where information for this QueryBuilder.
     */
    abstract fun where(): W

    /**
     * Set the [Where] object on the query. This allows someone to use the same Where object on multiple queries.
     */
    fun setWhere(where: W) {
        statementBuilder.setWhere(where.where)
    }

    /**
     * Build and return a string version of the query. If you change the where or make other calls you will need to
     * re-call this method to re-prepare the query for execution.
     */
    fun prepareStatementString(): Single<String> = Single.fromCallable { statementBuilder.prepareStatementString() }

    /**
     * Build and return all of the information about the prepared statement. See [StatementBuilder.StatementInfo] for more details.
     */
    fun prepareStatementInfo(): Single<StatementBuilder.StatementInfo> = Single.fromCallable { statementBuilder.prepareStatementInfo() }

    /**
     * Clear out all of the statement settings so we can reuse the builder.
     */
    fun reset() {
        statementBuilder.reset()
    }
}
