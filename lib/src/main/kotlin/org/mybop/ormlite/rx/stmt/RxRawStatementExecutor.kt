package org.mybop.ormlite.rx.stmt

import com.j256.ormlite.dao.ObjectCache
import com.j256.ormlite.db.DatabaseType
import com.j256.ormlite.stmt.GenericRowMapper
import com.j256.ormlite.support.ConnectionSource
import com.j256.ormlite.table.TableInfo
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Maybe
import org.mybop.ormlite.rx.stmt.publisher.PublisherBuilder

class RxRawStatementExecutor<T, ID> internal constructor(
        private val publisherBuilder: PublisherBuilder<T, ID>
) {

    constructor(
            databaseType: DatabaseType,
            tableInfo: TableInfo<T, ID>
    ) : this(
            PublisherBuilder(databaseType, tableInfo)
    )

    fun <R> queryRaw(connectionSource: ConnectionSource, query: String, args: Array<String>, rowMapper: GenericRowMapper<R>, objectCache: ObjectCache?): Flowable<R>
            = Flowable.fromPublisher(publisherBuilder.selectRaw(connectionSource, query, args, rowMapper, objectCache))

    fun <R> queryRawFirst(connectionSource: ConnectionSource, query: String, args: Array<String>, rowMapper: GenericRowMapper<R>, objectCache: ObjectCache?): Maybe<R>
            = Flowable.fromPublisher(publisherBuilder.selectRaw(connectionSource, query, args, rowMapper, objectCache, 1))
            .firstElement()

    fun executeRaw(connectionSource: ConnectionSource, query: String, args: Array<String>): Completable
            = Completable.fromPublisher(publisherBuilder.executeRaw(connectionSource, query, args))

    fun updateRaw(connectionSource: ConnectionSource, query: String, args: Array<String>): Completable
            = Completable.fromPublisher(publisherBuilder.updateRaw(connectionSource, query, args))
}
