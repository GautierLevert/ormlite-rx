package org.mybop.ormlite.rx.stmt

import com.j256.ormlite.dao.Dao
import com.j256.ormlite.db.DatabaseType
import com.j256.ormlite.stmt.PreparedQuery
import com.j256.ormlite.stmt.QueryBuilder
import com.j256.ormlite.table.TableInfo
import io.reactivex.Single

/**
 * Helper class preparing and eventually caching commonly used statements.
 *
 * @param T The class that the code will be operating on.
 * @param ID The class of the ID column associated with the class.
 */
internal class CommonlyUsedStatements<T, in ID>(
        private val databaseType: DatabaseType,
        private val tableInfo: TableInfo<T, ID>,
        private val dao: Dao<T, ID>
) {

    /**
     * [Single] emitting a [PreparedQuery] that matches all the element in the table.
     */
    val queryForAll: Single<PreparedQuery<T>> = Single.fromCallable { QueryBuilder(databaseType, tableInfo, dao).prepare() }
            .cache()

    /**
     * Returns a [Single] emitting a [PreparedQuery] that matches the element that has the same ID as the one provided in
     * parameter.
     *
     * @param id value used to filter rows on the ID field.
     */
    fun queryForId(id: ID): Single<PreparedQuery<T>> = Single.fromCallable {
        QueryBuilder(databaseType, tableInfo, dao)
                .where()
                .idEq(id)
                .prepare()
    }

    /**
     * [Single] emitting a [PreparedQuery] that returns the number of rows in the table.
     */
    val countAll: Single<PreparedQuery<T>> = Single.fromCallable { QueryBuilder(databaseType, tableInfo, dao).setCountOf(true).prepare() }
            .cache()
}
