package org.mybop.ormlite.rx.stmt.where

import com.j256.ormlite.stmt.ArgumentHolder
import com.j256.ormlite.stmt.PreparedStmt
import com.j256.ormlite.stmt.QueryBuilder
import com.j256.ormlite.stmt.Where
import io.reactivex.Single
import org.mybop.ormlite.rx.stmt.RxQueryBuilder
import org.mybop.ormlite.rx.stmt.RxStatementBuilder
import java.sql.SQLException

/**
 * <p>
 * Manages the various clauses that make up the WHERE part of a SQL statement. You get one of these when you call
 * [RxStatementBuilder.where] or you can set the where clause by calling [RxStatementBuilder.setWhere].
 * </p>
 *
 * <p>
 * Here's a page with a [good tutorial of SQL commands](http://www.w3schools.com/Sql/).
 * </p>
 *
 * <p>
 * To create a query which looks up an account by name and password you would do the following:
 * </p>
 *
 * <pre>
 * QueryBuilder&lt;Account, String&gt; qb = accountDao.queryBuilder();
 * Where where = qb.where();
 * // the name field must be equal to &quot;foo&quot;
 * where.eq(Account.NAME_FIELD_NAME, &quot;foo&quot;);
 * // and
 * where.and();
 * // the password field must be equal to &quot;_secret&quot;
 * where.eq(Account.PASSWORD_FIELD_NAME, &quot;_secret&quot;);
 * PreparedQuery&lt;Account, String&gt; preparedQuery = qb.prepareQuery();
 * </pre>
 *
 * <p>
 * In this example, the SQL query that will be generated will be approximately:
 * </p>
 *
 * <pre>
 * SELECT * FROM account WHERE (name = 'foo' AND password = '_secret')
 * </pre>
 *
 * <p>
 * If you'd rather chain the methods onto one line (like StringBuilder), this can also be written as:
 * </p>
 *
 * <pre>
 * queryBuilder.where().eq(Account.NAME_FIELD_NAME, &quot;foo&quot;).and().eq(Account.PASSWORD_FIELD_NAME, &quot;_secret&quot;);
 * </pre>
 *
 * <p>
 * If you'd rather use parents and the like then you can call:
 * </p>
 *
 * <pre>
 * Where where = queryBuilder.where();
 * where.and(where.eq(Account.NAME_FIELD_NAME, &quot;foo&quot;), where.eq(Account.PASSWORD_FIELD_NAME, &quot;_secret&quot;));
 * </pre>
 *
 * <p>
 * All three of the above call formats produce the same SQL. For complex queries that mix ANDs and ORs, the last format
 * will be necessary to get the grouping correct. For example, here's a complex query:
 * </p>
 *
 * <pre>
 * Where where = queryBuilder.where();
 * where.or(where.and(where.eq(Account.NAME_FIELD_NAME, &quot;foo&quot;), where.eq(Account.PASSWORD_FIELD_NAME, &quot;_secret&quot;)),
 * 		where.and(where.eq(Account.NAME_FIELD_NAME, &quot;bar&quot;), where.eq(Account.PASSWORD_FIELD_NAME, &quot;qwerty&quot;)));
 * </pre>
 *
 * <p>
 * This produces the following approximate SQL:
 * </p>
 *
 * <pre>
 * SELECT * FROM account WHERE ((name = 'foo' AND password = '_secret') OR (name = 'bar' AND password = 'qwerty'))
 * </pre>
 *
 * @see Where
 */
abstract class RxWhere<T, ID, R : RxWhere<T, ID, R>>(
        internal val where: Where<T, ID>
) {

    /**
     * AND operation which takes the previous clause and the next clause and AND's them together.
     */
    @Suppress("unchecked_cast")
    open fun and(): R {
        where.and()
        return this as R
    }

    /**
     * AND operation which takes 2 (or more) arguments and AND's them together.
     *
     * **NOTE:** There is no guarantee of the order of the clauses that are generated in the final query.
     *
     * **NOTE:** I can't remove the generics code warning that can be associated with this method. You can instead
     * use the [and] method.
     */
    @Suppress("unchecked_cast")
    open fun and(first: RxWhere<T, ID, *>, second: RxWhere<T, ID, *>, vararg others: RxWhere<T, ID, *>): R {
        where.and(first.where, second.where, *others.map { it.where }.toTypedArray())
        return this as R
    }

    /**
     * This method needs to be used carefully. This will absorb a number of clauses that were registered previously with
     * calls to [Where.eq] or other methods and will string them together with AND's. There is no
     * way to verify the number of previous clauses so the programmer has to count precisely.
     *
     * **NOTE:** There is no guarantee of the order of the clauses that are generated in the final query.
     *
     * **NOTE:** This will throw an exception if numClauses is 0 but will work with 1 or more.
     */
    @Suppress("unchecked_cast")
    open fun and(numClauses: Int): R {
        where.and(numClauses)
        return this as R
    }

    /**
     * Add a BETWEEN clause so the column must be between the low and high parameters.
     */
    @Throws(SQLException::class)
    @Suppress("unchecked_cast")
    open fun between(columnName: String, low: Any, high: Any): R {
        where.between(columnName, low, high)
        return this as R
    }

    /**
     * Add a '=' clause so the column must be equal to the value.
     */
    @Throws(SQLException::class)
    @Suppress("unchecked_cast")
    open fun eq(columnName: String, value: Any): R {
        where.eq(columnName, value)
        return this as R
    }

    /**
     * Add a '&gt;=' clause so the column must be greater-than or equals-to the value.
     */
    @Throws(SQLException::class)
    @Suppress("unchecked_cast")
    open fun ge(columnName: String, value: Any): R {
        where.ge(columnName, value)
        return this as R
    }

    /**
     * Add a '&gt;' clause so the column must be greater-than the value.
     */
    @Throws(SQLException::class)
    @Suppress("unchecked_cast")
    open fun gt(columnName: String, value: Any): R {
        where.gt(columnName, value)
        return this as R
    }

    /**
     * Add a IN clause so the column must be equal-to one of the objects from the list passed in.
     */
    @Throws(SQLException::class)
    @Suppress("unchecked_cast")
    open fun `in`(columnName: String, objects: Iterable<*>): R {
        where.`in`(columnName, objects)
        return this as R
    }

    /**
     * Same as [in] except with a NOT IN clause.
     */
    @Throws(SQLException::class)
    @Suppress("unchecked_cast")
    open fun notIn(columnName: String, objects: Iterable<*>): R {
        where.notIn(columnName, objects)
        return this as R
    }

    /**
     * Add a IN clause so the column must be equal-to one of the objects passed in.
     */
    @Throws(SQLException::class)
    @Suppress("unchecked_cast")
    open fun `in`(columnName: String, vararg objects: Any): R {
        where.`in`(columnName, *objects)
        return this as R
    }

    /**
     * Same as [in] except with a NOT IN clause.
     */
    @Throws(SQLException::class)
    @Suppress("unchecked_cast")
    open fun notIn(columnName: String, vararg objects: Any): R {
        where.notIn(columnName, *objects)
        return this as R
    }

    /**
     * Add a IN clause which makes sure the column is in one of the columns returned from a sub-query inside of
     * parenthesis. The QueryBuilder must return 1 and only one column which can be set with the
     * [QueryBuilder.selectColumns] method calls. That 1 argument must match the SQL type of the
     * column-name passed to this method.
     *
     * **NOTE:** The sub-query will be prepared at the same time that the outside query is.
     */
    @Throws(SQLException::class)
    @Suppress("unchecked_cast")
    open fun `in`(columnName: String, subQueryBuilder: RxQueryBuilder<*, *>): R {
        where.`in`(columnName, subQueryBuilder.queryBuilder)
        return this as R
    }

    /**
     * Same as [in] except with a NOT IN clause.
     */
    @Throws(SQLException::class)
    @Suppress("unchecked_cast")
    open fun notIn(columnName: String, subQueryBuilder: RxQueryBuilder<*, *>): R {
        where.notIn(columnName, subQueryBuilder.queryBuilder)
        return this as R
    }

    /**
     * Add a EXISTS clause with a sub-query inside of parenthesis.
     *
     * **NOTE:** The sub-query will be prepared at the same time that the outside query is.
     */
    @Suppress("unchecked_cast")
    open fun exists(subQueryBuilder: RxQueryBuilder<*, *>): R {
        where.exists(subQueryBuilder.queryBuilder)
        return this as R
    }

    /**
     * Add a 'IS NULL' clause so the column must be null. '=' NULL does not work.
     */
    @Throws(SQLException::class)
    @Suppress("unchecked_cast")
    open fun isNull(columnName: String): R {
        where.isNull(columnName)
        return this as R
    }

    /**
     * Add a 'IS NOT NULL' clause so the column must not be null. '&lt;&gt;' NULL does not work.
     */
    @Throws(SQLException::class)
    @Suppress("unchecked_cast")
    open fun isNotNull(columnName: String): R {
        where.isNotNull(columnName)
        return this as R
    }

    /**
     * Add a '&lt;=' clause so the column must be less-than or equals-to the value.
     */
    @Throws(SQLException::class)
    @Suppress("unchecked_cast")
    open fun le(columnName: String, value: Any): R {
        where.le(columnName, value)
        return this as R
    }

    /**
     * Add a '&lt;' clause so the column must be less-than the value.
     */
    @Throws(SQLException::class)
    @Suppress("unchecked_cast")
    open fun lt(columnName: String, value: Any): R {
        where.lt(columnName, value)
        return this as R
    }

    /**
     * Add a LIKE clause so the column must mach the value using '%' patterns.
     */
    @Throws(SQLException::class)
    @Suppress("unchecked_cast")
    open fun like(columnName: String, value: Any): R {
        where.like(columnName, value)
        return this as R
    }

    /**
     * Add a '&lt;&gt;' clause so the column must be not-equal-to the value.
     */
    @Throws(SQLException::class)
    @Suppress("unchecked_cast")
    open fun ne(columnName: String, value: Any): R {
        where.ne(columnName, value)
        return this as R
    }

    /**
     * Used to NOT the next clause specified.
     */
    @Suppress("unchecked_cast")
    open fun not(): R {
        where.not()
        return this as R
    }

    /**
     * Used to NOT the argument clause specified.
     */
    @Suppress("unchecked_cast")
    open fun not(comparison: RxWhere<T, ID, *>): R {
        where.not(comparison.where)
        return this as R
    }

    /**
     * OR operation which takes the previous clause and the next clause and OR's them together.
     */
    @Suppress("unchecked_cast")
    open fun or(): R {
        where.or()
        return this as R
    }

    /**
     * OR operation which takes 2 arguments and OR's them together.
     *
     * **NOTE:** There is no guarantee of the order of the clauses that are generated in the final query.
     *
     * **NOTE:** I can't remove the generics code warning that can be associated with this method. You can instead
     * use the [or] method.
     */
    @Suppress("unchecked_cast")
    open fun or(left: RxWhere<T, ID, *>, right: RxWhere<T, ID, *>, vararg others: RxWhere<T, ID, *>): R {
        where.or(left.where, right.where, *others.map { it.where }.toTypedArray())
        return this as R
    }

    /**
     * This method needs to be used carefully. This will absorb a number of clauses that were registered previously with
     * calls to [Where.eq] or other methods and will string them together with OR's. There is no
     * way to verify the number of previous clauses so the programmer has to count precisely.
     *
     * **NOTE:** There is no guarantee of the order of the clauses that are generated in the final query.
     *
     * **NOTE:** This will throw an exception if numClauses is 0 but will work with 1 or more.
     */
    @Suppress("unchecked_cast")
    open fun or(numClauses: Int): R {
        where.or(numClauses)
        return this as R
    }

    /**
     * Add a clause where the ID is equal to the argument.
     */
    @Throws(SQLException::class)
    @Suppress("unchecked_cast")
    open fun idEq(id: ID): R {
        where.idEq(id)
        return this as R
    }

    /**
     * Add a raw statement as part of the where that can be anything that the database supports. Using more structured
     * methods is recommended but this gives more control over the query and allows you to utilize database specific
     * features.
     *
     * @param rawStatement
     *            The statement that we should insert into the WHERE.
     *
     *
     * @param args
     *            Optional arguments that correspond to any ? specified in the rawStatement. Each of the arguments must
     *            have either the corresponding columnName or the sql-type set. **WARNING,** you cannot use the
     *            `SelectArg("columnName")` constructor since that sets the _value_, not the name. Use
     *            `new SelectArg("column-name", null);`.
     */
    @Suppress("unchecked_cast")
    open fun raw(rawStatement: String, vararg args: ArgumentHolder): R {
        where.raw(rawStatement, *args)
        return this as R
    }

    /**
     * Make a comparison where the operator is specified by the caller. It is up to the caller to specify an appropriate
     * operator for the database and that it be formatted correctly.
     */
    @Throws(SQLException::class)
    @Suppress("unchecked_cast")
    open fun rawComparison(columnName: String, rawOperator: String, value: Any): R {
        where.rawComparison(columnName, rawOperator, value)
        return this as R
    }

    /**
     * Reset the Where object so it can be re-used.
     */
    @Suppress("unchecked_cast")
    open fun reset(): R {
        where.reset()
        return this as R
    }

    /**
     * A short-cut for calling [RxStatementBuilder.prepare][org.mybop.ormlite.rx.stmt.RxStatementBuilder.prepare].
     */
    @Throws(SQLException::class)
    abstract fun prepare(): Single<out PreparedStmt<T>>

    /**
     * Returns the associated SQL WHERE statement.
     */
    fun getStatement(): Single<String> {
        return Single.fromCallable { where.statement }
    }
}
