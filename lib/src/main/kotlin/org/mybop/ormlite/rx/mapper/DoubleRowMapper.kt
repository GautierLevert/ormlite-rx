package org.mybop.ormlite.rx.mapper

import com.j256.ormlite.stmt.GenericRowMapper
import com.j256.ormlite.support.DatabaseResults

/**
 * A simple [GenericRowMapper] that retrieves the first column of the row as Double and returns it.
 */
object DoubleRowMapper : GenericRowMapper<Double> {
    override fun mapRow(results: DatabaseResults): Double = results.getDouble(0)
}
