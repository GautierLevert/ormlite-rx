package org.mybop.ormlite.rx.dao

import com.j256.ormlite.dao.RawRowObjectMapper
import com.j256.ormlite.field.DataType
import com.j256.ormlite.stmt.GenericRowMapper
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Maybe

interface RxRawDao {

    fun <R> queryRaw(query: String, rowMapper: GenericRowMapper<R>, vararg args: String): Flowable<R>

    fun <R> queryRaw(query: String, dataTypes: Array<DataType>, rowMapper: RawRowObjectMapper<R>, vararg args: String): Flowable<R>

    fun <R> queryRawFirst(query: String, rowMapper: GenericRowMapper<R>, vararg args: String): Maybe<R>

    fun executeRaw(query: String, vararg args: String): Completable

    fun updateRaw(query: String, vararg args: String): Completable
}
