package org.mybop.ormlite.rx.stmt.publisher.exception

import java.sql.SQLException

/**
 * Exception emitted to the publisher if a statement has not inserted/updated/deleted any row.
 */
class NoRowModifiedException : SQLException("No row has been inserted/updated/deleted when executing the statement.")
