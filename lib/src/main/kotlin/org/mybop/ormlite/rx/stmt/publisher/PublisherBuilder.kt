package org.mybop.ormlite.rx.stmt.publisher

import com.j256.ormlite.dao.ObjectCache
import com.j256.ormlite.db.DatabaseType
import com.j256.ormlite.stmt.GenericRowMapper
import com.j256.ormlite.stmt.PreparedDelete
import com.j256.ormlite.stmt.PreparedStmt
import com.j256.ormlite.stmt.PreparedUpdate
import com.j256.ormlite.stmt.mapped.*
import com.j256.ormlite.support.ConnectionSource
import com.j256.ormlite.table.TableInfo
import org.mybop.ormlite.rx.mapper.LongRowMapper
import org.mybop.ormlite.rx.stmt.publisher.compiled.execute.RawExecutePublisher
import org.mybop.ormlite.rx.stmt.publisher.compiled.select.RawSelectPublisher
import org.mybop.ormlite.rx.stmt.publisher.mapped.create.MappedCreatePublisher
import org.mybop.ormlite.rx.stmt.publisher.mapped.delete.MappedDeletePublisher
import org.mybop.ormlite.rx.stmt.publisher.mapped.deletecollection.MappedDeleteCollectionPublisher
import org.mybop.ormlite.rx.stmt.publisher.mapped.refresh.MappedRefreshPublisher
import org.mybop.ormlite.rx.stmt.publisher.mapped.update.MappedUpdatePublisher
import org.mybop.ormlite.rx.stmt.publisher.mapped.updateid.MappedUpdateIdPublisher
import org.mybop.ormlite.rx.stmt.publisher.prepared.select.SelectStatementPublisher
import org.mybop.ormlite.rx.stmt.publisher.prepared.ud.UpdateDeleteStatementPublisher

/**
 * This class will create [org.reactivestreams.Publisher] that will emits the results of the provided statement. It provide builder methods
 * for either [PreparedStmt] and [BaseMappedStatement].
 *
 * @param T The class that the code will be operating on.
 * @param ID The class of the ID column associated with the class.
 */
class PublisherBuilder<T, ID>(
        private val databaseType: DatabaseType,
        private val tableInfo: TableInfo<T, ID>
) {

    fun select(
            connectionSource: ConnectionSource,
            preparedStmt: PreparedStmt<T>,
            objectCache: ObjectCache?,
            maxRows: Int? = null
    ) = SelectStatementPublisher(connectionSource, tableInfo.tableName, preparedStmt, preparedStmt, objectCache, maxRows)

    fun <R> selectRaw(
            connectionSource: ConnectionSource,
            query: String,
            arguments: Array<String>,
            rowMapper: GenericRowMapper<R>,
            objectCache: ObjectCache? = null,
            maxRows: Int? = null
    ) = RawSelectPublisher(connectionSource, tableInfo.tableName, query, arguments, rowMapper, objectCache, maxRows)

    fun count(
            connectionSource: ConnectionSource,
            preparedStmt: PreparedStmt<T>
    ) = SelectStatementPublisher(connectionSource, tableInfo.tableName, preparedStmt, LongRowMapper, null, 1)

    fun create(
            connectionSource: ConnectionSource,
            mappedCreate: MappedCreate<T, ID>,
            data: T,
            objectCache: ObjectCache?
    ) = MappedCreatePublisher(connectionSource, tableInfo.tableName, mappedCreate, data, objectCache)

    fun update(
            connectionSource: ConnectionSource,
            mappedUpdate: MappedUpdate<T, ID>,
            data: T,
            objectCache: ObjectCache?
    ) = MappedUpdatePublisher(connectionSource, tableInfo.tableName, mappedUpdate, data, objectCache)

    fun updateId(
            connectionSource: ConnectionSource,
            mappedUpdate: MappedUpdateId<T, ID>,
            data: T,
            newId: ID,
            objectCache: ObjectCache?
    ) = MappedUpdateIdPublisher(connectionSource, tableInfo.tableName, mappedUpdate, data, newId, objectCache)

    fun preparedUpdate(
            connectionSource: ConnectionSource,
            preparedUpdate: PreparedUpdate<T>
    ) = UpdateDeleteStatementPublisher(connectionSource, tableInfo.tableName, preparedUpdate)

    fun refresh(
            connectionSource: ConnectionSource,
            mappedRefresh: MappedRefresh<T, ID>,
            data: T,
            objectCache: ObjectCache?
    ) = MappedRefreshPublisher(connectionSource, tableInfo.tableName, mappedRefresh, data, objectCache)

    fun delete(
            connectionSource: ConnectionSource,
            mappedDelete: MappedDelete<T, ID>,
            data: T,
            objectCache: ObjectCache?
    ) = MappedDeletePublisher(connectionSource, tableInfo.tableName, mappedDelete, data, null, objectCache)

    fun deleteById(
            connectionSource: ConnectionSource,
            mappedDelete: MappedDelete<T, ID>,
            id: ID,
            objectCache: ObjectCache?
    ) = MappedDeletePublisher(connectionSource, tableInfo.tableName, mappedDelete, null, id, objectCache)

    fun deleteObjects(
            connectionSource: ConnectionSource,
            dataCollection: Collection<T>,
            objectCache: ObjectCache?
    ) = MappedDeleteCollectionPublisher(databaseType, tableInfo, connectionSource, dataCollection, null, objectCache)

    fun deleteIds(
            connectionSource: ConnectionSource,
            idCollection: Collection<ID>,
            objectCache: ObjectCache?
    ) = MappedDeleteCollectionPublisher(databaseType, tableInfo, connectionSource, null, idCollection, objectCache)

    fun preparedDelete(
            connectionSource: ConnectionSource,
            preparedDelete: PreparedDelete<T>
    ) = UpdateDeleteStatementPublisher(connectionSource, tableInfo.tableName, preparedDelete)

    fun executeRaw(
            connectionSource: ConnectionSource,
            query: String,
            args: Array<String>
    ) = RawExecutePublisher(connectionSource, query, args, false)

    fun updateRaw(
            connectionSource: ConnectionSource,
            query: String,
            args: Array<String>
    ) = RawExecutePublisher(connectionSource, query, args, true)
}
