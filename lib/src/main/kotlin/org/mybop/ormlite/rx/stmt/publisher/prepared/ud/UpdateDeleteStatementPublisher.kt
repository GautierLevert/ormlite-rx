package org.mybop.ormlite.rx.stmt.publisher.prepared.ud

import com.j256.ormlite.stmt.PreparedStmt
import com.j256.ormlite.stmt.StatementBuilder
import com.j256.ormlite.support.CompiledStatement
import com.j256.ormlite.support.ConnectionSource
import com.j256.ormlite.support.DatabaseConnection
import io.reactivex.internal.subscriptions.EmptySubscription
import org.mybop.ormlite.rx.stmt.publisher.compiled.execute.CompiledExecuteSubscription
import org.mybop.ormlite.rx.stmt.publisher.prepared.select.SelectStatementPublisher
import org.mybop.ormlite.rx.support.releaseConnectionQuietly
import org.reactivestreams.Publisher
import org.reactivestreams.Subscriber
import java.sql.SQLException

/**
 * Publisher that execute the provided prepared statement and publishes the number of rows modified/deleted.
 *
 * @param T The class that the code will be operating on.
 * @param connectionSource An open [ConnectionSource] to the database.
 * @param tableName Name of the table in which we will update/delete rows.
 * @param preparedStmt Statement to execute.
 */
class UpdateDeleteStatementPublisher<T>(
        private val connectionSource: ConnectionSource,
        private val tableName: String,
        private val preparedStmt: PreparedStmt<T>
) : Publisher<Int> {

    companion object {
        val supportedStatementTypes = listOf(StatementBuilder.StatementType.UPDATE, StatementBuilder.StatementType.DELETE)
    }

    init {
        if (!supportedStatementTypes.contains(preparedStmt.type)) {
            throw IllegalStateException("${SelectStatementPublisher::class.java.simpleName} only supports UPDATE and DELETE queries.")
        }
    }

    override fun subscribe(subscriber: Subscriber<in Int>) {
        var databaseConnection: DatabaseConnection? = null
        var compiledStatement: CompiledStatement? = null
        try {
            databaseConnection = connectionSource.getReadOnlyConnection(tableName)
            compiledStatement = preparedStmt.compile(databaseConnection, preparedStmt.type)
            subscriber.onSubscribe(CompiledExecuteSubscription(
                    subscriber,
                    connectionSource,
                    databaseConnection!!,
                    compiledStatement,
                    true
            ))
        } catch (e: SQLException) {
            connectionSource.releaseConnectionQuietly(databaseConnection)
            compiledStatement?.closeQuietly()
            EmptySubscription.error(e, subscriber)
        }
    }
}
