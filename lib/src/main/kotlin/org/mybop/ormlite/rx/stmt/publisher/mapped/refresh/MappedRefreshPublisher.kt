package org.mybop.ormlite.rx.stmt.publisher.mapped.refresh

import com.j256.ormlite.dao.ObjectCache
import com.j256.ormlite.stmt.mapped.MappedRefresh
import com.j256.ormlite.support.ConnectionSource
import com.j256.ormlite.support.DatabaseConnection
import org.mybop.ormlite.rx.stmt.publisher.mapped.MappedStatementPublisher
import org.reactivestreams.Subscriber

/**
 * Publisher that will refresh the data from the database and emits them to the publisher.
 *
 * The data parameter will be modified by the operation.
 *
 * @param T The class that the code will be operating on.
 * @param ID The class of the ID column associated with the class.
 * @param connectionSource An open [ConnectionSource] to the database.
 * @param tableName Name of the table in which the data are located.
 * @param mappedStatement Refresh statement to execute when the publisher subscribes and requests at least one element.
 * @param data Data to refresh from the database. ID field must be set.
 * @param objectCache Optional [ObjectCache].
 *
 * @see MappedStatementPublisher
 * @see MappedRefreshSubscription
 */
class MappedRefreshPublisher<T, ID>(
        connectionSource: ConnectionSource,
        tableName: String,
        mappedStatement: MappedRefresh<T, ID>,
        private val data: T,
        objectCache: ObjectCache?
) : MappedStatementPublisher<T, ID, T, MappedRefresh<T, ID>, MappedRefreshSubscription<T, ID>>(
        connectionSource, tableName, mappedStatement, objectCache
) {
    override fun createSubscription(
            subscriber: Subscriber<in T>,
            connectionSource: ConnectionSource,
            connection: DatabaseConnection,
            mappedStatement: MappedRefresh<T, ID>,
            objectCache: ObjectCache?): MappedRefreshSubscription<T, ID>
            = MappedRefreshSubscription(subscriber, connectionSource, connection, mappedStatement, data, objectCache)
}
