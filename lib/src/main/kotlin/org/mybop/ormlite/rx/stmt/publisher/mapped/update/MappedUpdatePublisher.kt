package org.mybop.ormlite.rx.stmt.publisher.mapped.update

import com.j256.ormlite.dao.ObjectCache
import com.j256.ormlite.stmt.mapped.MappedUpdate
import com.j256.ormlite.support.ConnectionSource
import com.j256.ormlite.support.DatabaseConnection
import org.mybop.ormlite.rx.stmt.publisher.mapped.MappedStatementPublisher
import org.reactivestreams.Subscriber

/**
 * Publisher that will execute the update and emits the number of rows modified in the database.
 *
 * @param T The class that the code will be operating on.
 * @param ID The class of the ID column associated with the class.
 * @param connectionSource An open [ConnectionSource] to the database.
 * @param tableName Table on which the update will be executed.
 * @param mappedStatement Update statement to execute when the publisher subscribes and requests at least one element.
 * @param data Data to update in the database. ID field must be set.
 * @param objectCache Optional [ObjectCache].
 *
 * @see [MappedStatementPublisher]
 * @see [MappedUpdateSubscription]
 */
class MappedUpdatePublisher<T, ID>(
        connectionSource: ConnectionSource,
        tableName: String,
        mappedStatement: MappedUpdate<T, ID>,
        private val data: T,
        objectCache: ObjectCache?
) : MappedStatementPublisher<T, ID, Int, MappedUpdate<T, ID>, MappedUpdateSubscription<T, ID>>(
        connectionSource, tableName, mappedStatement, objectCache
) {
    override fun createSubscription(
            subscriber: Subscriber<in Int>,
            connectionSource: ConnectionSource,
            connection: DatabaseConnection,
            mappedStatement: MappedUpdate<T, ID>,
            objectCache: ObjectCache?)
            = MappedUpdateSubscription(subscriber, connectionSource, connection, mappedStatement, data, objectCache)
}
