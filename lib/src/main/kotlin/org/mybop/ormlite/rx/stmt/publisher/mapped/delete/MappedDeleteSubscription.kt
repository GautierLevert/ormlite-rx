package org.mybop.ormlite.rx.stmt.publisher.mapped.delete

import com.j256.ormlite.dao.ObjectCache
import com.j256.ormlite.stmt.mapped.MappedDelete
import com.j256.ormlite.support.ConnectionSource
import com.j256.ormlite.support.DatabaseConnection
import org.mybop.ormlite.rx.stmt.publisher.mapped.MappedStatementSubscription
import org.reactivestreams.Subscriber

/**
 * A subscription that will execute the delete and emit the number of rows deleted to
 * the publisher.
 *
 * @param T The class that the code will be operating on.
 * @param ID The class of the ID column associated with the class.
 * @param subscriber [Subscriber] to emit the number of rows to.
 * @param connectionSource [ConnectionSource] that have provided the connection.
 * @param connection [DatabaseConnection] on which the mapped statement will be executed.
 * @param mappedStatement Create statement that will be executed when a publisher subscribes and requests.
 * @param data The data to delete with its ID field set. Either this or id parameter must be provided.
 * @param id The ID of the data to delete. Either this or data parameter must be provided.
 * @param objectCache Optional [ObjectCache].
 *
 * @see MappedStatementSubscription
 */
class MappedDeleteSubscription<T, ID>(
        subscriber: Subscriber<in Int>,
        connectionSource: ConnectionSource,
        connection: DatabaseConnection,
        mappedStatement: MappedDelete<T, ID>,
        private val data: T?,
        private val id: ID?,
        objectCache: ObjectCache?
) : MappedStatementSubscription<T, ID, Int, MappedDelete<T, ID>>(subscriber, connectionSource, connection,
        mappedStatement, objectCache) {

    init {
        if (data == null && id == null) {
            throw IllegalArgumentException("Either data or id must be specified.")
        }
    }

    override fun execute(
            mappedStatement: MappedDelete<T, ID>,
            connection: DatabaseConnection,
            objectCache: ObjectCache?
    ): Int =
            if (id != null) {
                mappedStatement.deleteById(connection, id, objectCache)
            } else {
                mappedStatement.delete(connection, data, objectCache)
            }
}
