package org.mybop.ormlite.rx.stmt.where

import com.j256.ormlite.stmt.GenericRowMapper
import com.j256.ormlite.stmt.PreparedQuery
import com.j256.ormlite.stmt.QueryBuilder
import com.j256.ormlite.stmt.Where
import io.reactivex.Flowable
import io.reactivex.Maybe
import io.reactivex.Single
import org.mybop.ormlite.rx.stmt.RxQueryBuilder

class RxQueryWhere<T, ID>(
        where: Where<T, ID>,
        private val queryBuilder: RxQueryBuilder<T, ID>
) : RxWhere<T, ID, RxQueryWhere<T, ID>>(where) {

    override fun prepare(): Single<PreparedQuery<T>> = queryBuilder.prepare()

    /**
     * A short-cut for calling [RxQueryBuilder.query].
     */
    fun query(): Flowable<T> = queryBuilder.query()

    /**
     * A short-cut for calling [QueryBuilder.queryRaw].
     */
    fun queryRaw(): Flowable<Array<String>> = queryBuilder.queryRaw()

    /**
     * A short-cut for calling [QueryBuilder.queryRaw].
     */
    fun <R> queryRaw(rowMapper: GenericRowMapper<R>): Flowable<R>
            = queryBuilder.queryRaw(rowMapper)

    /**
     * A short-cut for calling [QueryBuilder.queryForFirst].
     */
    fun queryForFirst(): Maybe<T> = queryBuilder.queryForFirst()

    /**
     * A short-cut for calling [QueryBuilder.queryRawFirst].
     */
    fun queryRawFirst(): Maybe<Array<String>> = queryBuilder.queryRawFirst()

    /**
     * A short-cut for calling [QueryBuilder.queryRawFirst].
     */
    fun <R> queryRawFirst(rowMapper: GenericRowMapper<R>): Maybe<R>
            = queryBuilder.queryRawFirst(rowMapper)

    /**
     * A short-cut for calling [QueryBuilder.countOf].
     */
    fun countOf(): Single<Long> = queryBuilder.countOf()

}
