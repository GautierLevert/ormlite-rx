package org.mybop.ormlite.rx.stmt.publisher.exception

import java.sql.SQLException

/**
 * Exception emitted to the publisher if the data provided was not refreshed from database.
 */
class NotRefreshedException : SQLException("Data cannot be refreshed from database.")
