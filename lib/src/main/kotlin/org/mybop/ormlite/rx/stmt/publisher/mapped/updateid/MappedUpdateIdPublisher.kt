package org.mybop.ormlite.rx.stmt.publisher.mapped.updateid

import com.j256.ormlite.dao.ObjectCache
import com.j256.ormlite.stmt.mapped.MappedUpdateId
import com.j256.ormlite.support.ConnectionSource
import com.j256.ormlite.support.DatabaseConnection
import org.mybop.ormlite.rx.stmt.publisher.mapped.MappedStatementPublisher
import org.reactivestreams.Subscriber

/**
 * Publisher that will execute the update and emits the number of rows modified in the database.
 *
 * @param T The class that the code will be operating on.
 * @param ID The class of the ID column associated with the class.
 * @param connectionSource An open [ConnectionSource] to the database.
 * @param tableName Table on which the update will be executed.
 * @param mappedStatement Update statement to execute when the publisher subscribes and requests at least one element.
 * @param data Date to update in the database. ID field must be set.
 * @param newId New id to set in database for the data parameter.
 * @param objectCache Optional [ObjectCache].
 *
 * @see MappedStatementPublisher
 */
class MappedUpdateIdPublisher<T, ID>(
        connectionSource: ConnectionSource,
        tableName: String,
        mappedStatement: MappedUpdateId<T, ID>,
        private val data: T,
        private val newId: ID,
        objectCache: ObjectCache?
) : MappedStatementPublisher<T, ID, Int, MappedUpdateId<T, ID>, MappedUpdateIdSubscription<T, ID>>(
        connectionSource, tableName, mappedStatement, objectCache
) {
    override fun createSubscription(
            subscriber: Subscriber<in Int>,
            connectionSource: ConnectionSource,
            connection: DatabaseConnection,
            mappedStatement: MappedUpdateId<T, ID>,
            objectCache: ObjectCache?): MappedUpdateIdSubscription<T, ID>
            = MappedUpdateIdSubscription(subscriber, connectionSource, connection, mappedStatement, data, newId, objectCache)
}
