package org.mybop.ormlite.rx.stmt.publisher.mapped.create

import com.j256.ormlite.dao.ObjectCache
import com.j256.ormlite.stmt.mapped.MappedCreate
import com.j256.ormlite.support.ConnectionSource
import com.j256.ormlite.support.DatabaseConnection
import org.mybop.ormlite.rx.stmt.publisher.mapped.MappedStatementPublisher
import org.reactivestreams.Subscriber

/**
 * Publisher that will insert the data and emits the number of rows inserted in the database.
 *
 * @param T The class that the code will be operating on.
 * @param ID The class of the ID column associated with the class.
 * @param connectionSource An open [ConnectionSource] to the database.
 * @param tableName Table on which the insert will be executed.
 * @param mappedStatement Insert statement to execute when the publisher subscribes and requests at least one element.
 * @param data Data to create in database.
 * @param objectCache Optional [ObjectCache].
 *
 * @see MappedStatementPublisher
 * @see MappedCreateSubscription
 */
class MappedCreatePublisher<T, ID>(
        connectionSource: ConnectionSource,
        tableName: String,
        mappedStatement: MappedCreate<T, ID>,
        private val data: T,
        objectCache: ObjectCache?
) : MappedStatementPublisher<T, ID, Int, MappedCreate<T, ID>, MappedCreateSubscription<T, ID>>(
        connectionSource, tableName, mappedStatement, objectCache
) {
    override fun createSubscription(
            subscriber: Subscriber<in Int>,
            connectionSource: ConnectionSource,
            connection: DatabaseConnection,
            mappedStatement: MappedCreate<T, ID>,
            objectCache: ObjectCache?): MappedCreateSubscription<T, ID>
            = MappedCreateSubscription(subscriber, connectionSource, connection, mappedStatement, data, objectCache)
}
