package org.mybop.ormlite.rx.mapper

import com.j256.ormlite.stmt.GenericRowMapper
import com.j256.ormlite.support.DatabaseResults

/**
 * A simple [GenericRowMapper] that retrieves the first column of the row as String and returns it.
 */
object StringRowMapper : GenericRowMapper<String> {
    override fun mapRow(results: DatabaseResults): String = results.getString(0)
}
