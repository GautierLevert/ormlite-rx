package org.mybop.ormlite.rx.mapper

import com.j256.ormlite.stmt.GenericRowMapper
import com.j256.ormlite.support.DatabaseResults

/**
 * A simple [GenericRowMapper] that retrieves the first column of the row as long and returns it.
 */
object StringArrayRowMapper : GenericRowMapper<Array<String>> {

    override fun mapRow(results: DatabaseResults): Array<String>
            = (0 until results.columnCount)
            .map { results.getString(it) }
            .toTypedArray()
}
