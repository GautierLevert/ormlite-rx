package org.mybop.ormlite.rx.stmt.where

import com.j256.ormlite.dao.Dao
import com.j256.ormlite.stmt.PreparedUpdate
import com.j256.ormlite.stmt.Where
import io.reactivex.Completable
import io.reactivex.Single
import org.mybop.ormlite.rx.stmt.RxUpdateBuilder

class RxUpdateWhere<T, ID>(
        where: Where<T, ID>,
        private val updateBuilder: RxUpdateBuilder<T, ID>
) : RxWhere<T, ID, RxUpdateWhere<T, ID>>(where) {

    override fun prepare(): Single<out PreparedUpdate<T>> = updateBuilder.prepare()

    /**
     * A short cut to [Dao.update].
     */
    fun update(): Completable = updateBuilder.update()
}
