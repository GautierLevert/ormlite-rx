package org.mybop.ormlite.rx.dao

import com.j256.ormlite.dao.ObjectCache
import java.sql.SQLException

/**
 * Operations allowing to enable/disable the usage of an [ObjectCache] by [RxDao] operations.
 */
interface ObjectCacheManager {

    /**
     * Set the actual cache instance to use for the DAO. This allows you to use a [ReferenceObjectCache][com.j256.ormlite.dao.ReferenceObjectCache]
     * with [SoftReference][java.lang.ref.SoftReference] setting, the [LruObjectCache][com.j256.ormlite.dao.LruObjectCache],
     * or inject your own cache implementation. Set it to null to disable the cache.
     *
     * @throws SQLException If the DAO's class does not have an id field which is required by the [ObjectCache].
     */
    var objectCache: ObjectCache?
        @Throws(SQLException::class) set
}
