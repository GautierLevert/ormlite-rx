package org.mybop.ormlite.rx.stmt.publisher.mapped.updateid

import com.j256.ormlite.dao.ObjectCache
import com.j256.ormlite.stmt.mapped.MappedUpdateId
import com.j256.ormlite.support.ConnectionSource
import com.j256.ormlite.support.DatabaseConnection
import org.mybop.ormlite.rx.stmt.publisher.mapped.MappedStatementSubscription
import org.reactivestreams.Subscriber

/**
 * A subscription that will execute the update and emit the number of rows updated to
 * the publisher.
 *
 * @param T The class that the code will be operating on.
 * @param ID The class of the ID column associated with the class.
 * @param subscriber [Subscriber] to emit the number of rows to.
 * @param connectionSource [ConnectionSource] that have provided the connection.
 * @param connection [DatabaseConnection] on which the mapped statement will be executed.
 * @param mappedStatement Mapped statement that will be executed when a publisher subscribes and requests at least one element.
 * @param data Date to update in the database. ID field must be set.
 * @param newId New id to set in database for the data parameter.
 * @param objectCache Optional [ObjectCache].
 *
 * @see MappedStatementSubscription
 */
class MappedUpdateIdSubscription<T, ID>(
        subscriber: Subscriber<in Int>,
        connectionSource: ConnectionSource,
        connection: DatabaseConnection,
        mappedStatement: MappedUpdateId<T, ID>,
        private val data: T,
        private val newId: ID,
        objectCache: ObjectCache?
) : MappedStatementSubscription<T, ID, Int, MappedUpdateId<T, ID>>(
        subscriber, connectionSource, connection, mappedStatement, objectCache
) {
    override fun execute(mappedStatement: MappedUpdateId<T, ID>,
                         connection: DatabaseConnection,
                         objectCache: ObjectCache?): Int = mappedStatement.execute(connection, data, newId, objectCache)
}
