package org.mybop.ormlite.rx.stmt.publisher.mapped.deletecollection

import com.j256.ormlite.dao.ObjectCache
import com.j256.ormlite.db.DatabaseType
import com.j256.ormlite.stmt.mapped.MappedDeleteCollection
import com.j256.ormlite.support.ConnectionSource
import com.j256.ormlite.support.DatabaseConnection
import com.j256.ormlite.table.TableInfo
import org.mybop.ormlite.rx.stmt.publisher.ConnectionBackedSubscription
import org.mybop.ormlite.rx.stmt.publisher.mapped.MappedStatementSubscription
import org.reactivestreams.Subscriber

/**
 * A subscription that will execute the delete and emit the number of rows deleted to
 * the publisher.
 *
 * @param T The class that the code will be operating on.
 * @param ID The class of the ID column associated with the class.
 * @param subscriber [Subscriber] to emit the number of rows to.
 * @param databaseType Type of the database on which the mapped statement will be executed.
 * @param tableInfo [TableInfo] of the table from which we will delete the items.
 * @param connectionSource An open [ConnectionSource] to the database.
 * @param dataCollection Collection of data to delete with their ID field set. Either this or idCollection parameter must be provided.
 * @param idCollection Collection of ID to delete. Either this or dataCollection parameter must be provided.
 * @param objectCache Optional [ObjectCache].
 *
 * @see MappedStatementSubscription
 */
class MappedDeleteCollectionSubscription<T, ID>(
        private val subscriber: Subscriber<in Int>,
        private val databaseType: DatabaseType,
        private val tableInfo: TableInfo<T, ID>,
        connectionSource: ConnectionSource,
        private val connection: DatabaseConnection,
        private val dataCollection: Collection<T>?,
        private val idCollection: Collection<ID>?,
        private val objectCache: ObjectCache?
) : ConnectionBackedSubscription<Int>(subscriber, connectionSource, connection) {
    init {
        if (dataCollection == null && idCollection == null) {
            throw IllegalArgumentException("Either dataCollection or idCollection must be specified.")
        }
    }

    override fun doRequest(n: Long) {
        val result = if (idCollection != null) {
            MappedDeleteCollection.deleteIds(databaseType, tableInfo, connection, idCollection, objectCache)
        } else {
            MappedDeleteCollection.deleteObjects(databaseType, tableInfo, connection, dataCollection, objectCache)
        }
        subscriber.onNext(result)
        subscriber.onComplete()
    }

    override fun close() {}
}
