package org.mybop.ormlite.rx.stmt.where

import com.j256.ormlite.stmt.PreparedDelete
import com.j256.ormlite.stmt.Where
import io.reactivex.Completable
import io.reactivex.Single
import org.mybop.ormlite.rx.stmt.RxDeleteBuilder

class RxDeleteWhere<T, ID>(
        where: Where<T, ID>,
        private val deleteBuilder: RxDeleteBuilder<T, ID>
) : RxWhere<T, ID, RxDeleteWhere<T, ID>>(where) {

    override fun prepare(): Single<PreparedDelete<T>> = deleteBuilder.prepare()

    /**
     * A short cut to [RxDeleteBuilder.delete].
     */
    fun delete(): Completable = deleteBuilder.delete()
}
