package org.mybop.ormlite.rx.stmt.publisher.mapped.delete

import com.j256.ormlite.dao.ObjectCache
import com.j256.ormlite.stmt.mapped.MappedDelete
import com.j256.ormlite.support.ConnectionSource
import com.j256.ormlite.support.DatabaseConnection
import org.mybop.ormlite.rx.stmt.publisher.mapped.MappedStatementPublisher
import org.reactivestreams.Subscriber

/**
 * Publisher that will delete the data and emits the number of rows deleted from the database.
 *
 * @param T The class that the code will be operating on.
 * @param ID The class of the ID column associated with the class.
 * @param connectionSource An open [ConnectionSource] to the database.
 * @param tableName Table on which the delete will be executed.
 * @param mappedStatement Delete statement to execute when the publisher subscribes and requests at least one element.
 * @param data The data to delete with its ID field set. Either this or id parameter must be provided.
 * @param id The ID of the data to delete. Either this or data parameter must be provided.
 * @param objectCache Optional [ObjectCache].
 *
 * @see MappedStatementPublisher
 * @see MappedDeleteSubscription
 */
class MappedDeletePublisher<T, ID>(
        connectionSource: ConnectionSource,
        tableName: String,
        mappedStatement: MappedDelete<T, ID>,
        private val data: T?,
        private val id: ID?,
        objectCache: ObjectCache?
) : MappedStatementPublisher<T, ID, Int, MappedDelete<T, ID>, MappedDeleteSubscription<T, ID>>(
        connectionSource, tableName, mappedStatement, objectCache
) {
    init {
        if (data == null && id == null) {
            throw IllegalStateException("Either data or id must be specified.")
        }
    }

    override fun createSubscription(
            subscriber: Subscriber<in Int>,
            connectionSource: ConnectionSource,
            connection: DatabaseConnection,
            mappedStatement: MappedDelete<T, ID>,
            objectCache: ObjectCache?): MappedDeleteSubscription<T, ID>
            = MappedDeleteSubscription(subscriber, connectionSource, connection, mappedStatement, data, id, objectCache)
}
