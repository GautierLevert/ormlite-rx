package org.mybop.ormlite.rx.example.onetomany

import com.j256.ormlite.field.DatabaseField
import com.j256.ormlite.table.DatabaseTable

@DatabaseTable(tableName = "message")
data class Message(
        @DatabaseField(generatedId = true)
        val id: Long = 0,
        @DatabaseField
        val content: String = "",
        @DatabaseField(foreign = true)
        val creator: Account? = null
)
