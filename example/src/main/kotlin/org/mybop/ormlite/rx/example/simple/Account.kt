package org.mybop.ormlite.rx.example.simple

import com.j256.ormlite.field.DatabaseField
import com.j256.ormlite.table.DatabaseTable

@DatabaseTable(tableName = "account")
data class Account(
        @DatabaseField(generatedId = true)
        val id: Int = 0,

        @DatabaseField(columnName = NAME_COLUMN_NAME, canBeNull = false)
        var name: String = "",

        @DatabaseField(columnName = PASSWORD_COLUMN_NAME)
        var password: String = ""
) {

    companion object {
        const val NAME_COLUMN_NAME = "name"
        const val PASSWORD_COLUMN_NAME = "password"
    }
}
