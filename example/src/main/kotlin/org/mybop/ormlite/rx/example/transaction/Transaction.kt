package org.mybop.ormlite.rx.example.transaction

import com.j256.ormlite.jdbc.JdbcConnectionSource
import com.j256.ormlite.table.TableUtils
import io.reactivex.Completable
import org.mybop.ormlite.rx.dao.RxDaoManager
import org.mybop.ormlite.rx.misc.RxTransactionManager

const val DATABASE_URL = "jdbc:h2:mem:account"

fun main(vararg args: String) {
    JdbcConnectionSource(DATABASE_URL).use { connectionSource ->
        val accountDao = RxDaoManager.createDao<Account, Int>(connectionSource, Account::class.java)

        // if you need to create the table
        TableUtils.createTable(connectionSource, Account::class.java)

        val transactionManager = RxTransactionManager(connectionSource)

        // create an instance of Account
        val name = "Jim Coakley"
        val account = Account(name = name)

        // persist the account object to the database
        accountDao.create(account).subscribe()

        // assign a password
        account.password = "_secret"

        // update the database after changing the object
        accountDao.update(account).subscribe()

        accountDao.queryForAll().subscribe {
            println(it)
        }

        // delete the account but send exception inside of transaction
        transactionManager.callInTransaction(
                accountDao.delete(account)
                        .andThen(Completable.error(IllegalStateException()))
        ).subscribe({

        }, {
            // this is normal to get the exception here
        })

        // user is still here
        accountDao.queryForId(account.id).subscribe {
            println("account ${Account.NAME_COLUMN_NAME} LIKE hello: $it")
        }

        // delete the account (really this time)
        transactionManager.callInTransaction(
                accountDao.delete(account)
        ).subscribe()

        // user has disappeared
        accountDao.queryForId(account.id).subscribe {
            println("account ${Account.NAME_COLUMN_NAME} LIKE hello: $it")
        }
    }
}
