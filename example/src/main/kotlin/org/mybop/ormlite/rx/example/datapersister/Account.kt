package org.mybop.ormlite.rx.example.datapersister

import com.j256.ormlite.field.DatabaseField
import org.joda.time.Instant

data class Account(
        @DatabaseField(id = true)
        val creationDate: Instant = Instant.now(),
        @DatabaseField(columnName = NAME_COLUMN_NAME)
        var name: String = "",
        @DatabaseField(columnName = PASSWORD_COLUMN_NAME)
        var password: String = ""
) {
    companion object {
        const val NAME_COLUMN_NAME = "name"
        const val PASSWORD_COLUMN_NAME = "password"
    }
}
