package org.mybop.ormlite.rx.example.onetomany

import com.j256.ormlite.dao.ForeignCollection
import com.j256.ormlite.field.DatabaseField
import com.j256.ormlite.field.ForeignCollectionField
import com.j256.ormlite.table.DatabaseTable

@DatabaseTable(tableName = "account")
data class Account(
        @DatabaseField(generatedId = true)
        val id: Int = 0,

        @DatabaseField(columnName = NAME_COLUMN_NAME, canBeNull = false)
        var name: String = "",

        @DatabaseField(columnName = PASSWORD_COLUMN_NAME)
        var password: String = ""
) {

    @ForeignCollectionField(eager = true)
    val messages: ForeignCollection<Message>? = null

    companion object {
        const val NAME_COLUMN_NAME = "name"
        const val PASSWORD_COLUMN_NAME = "password"
    }
}
