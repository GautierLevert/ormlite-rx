package org.mybop.ormlite.rx.example.simple

import com.j256.ormlite.jdbc.JdbcConnectionSource
import com.j256.ormlite.table.TableUtils
import org.mybop.ormlite.rx.dao.RxDaoManager

const val DATABASE_URL = "jdbc:h2:mem:account"

fun main(vararg args: String) {
    JdbcConnectionSource(DATABASE_URL).use { connectionSource ->
        val accountDao = RxDaoManager.createDao<Account, Int>(connectionSource, Account::class.java)

        // if you need to create the table
        TableUtils.createTable(connectionSource, Account::class.java)

        // create an instance of Account
        val name = "Jim Coakley"
        val account = Account(name = name)

        // persist the account object to the database
        accountDao.create(account).subscribe()

        // assign a password
        account.password = "_secret"

        // update the database after changing the object
        accountDao.update(account).subscribe()

        accountDao.queryForAll().subscribe {
            println(it)
        }

        // shouldn't find anything: name LIKE 'hello" does not match our account
        accountDao.queryBuilder().where()
                .like(Account.NAME_COLUMN_NAME, "hello")
                .countOf()
                .subscribe { count ->
                    println("count ${Account.NAME_COLUMN_NAME} LIKE hello: $count")
                }

        // should find our account: name LIKE 'Jim%' should match our account
        accountDao.queryBuilder()
                .where()
                .like(Account.NAME_COLUMN_NAME, "${name.substring(0, 3)}%")
                .query()
                .subscribe {
                    println("account with ${Account.NAME_COLUMN_NAME} LIKE ${name.substring(0, 3) + "%"}: $it")
                }

        // delete the account since we are done with it
        accountDao.delete(account).subscribe()

        // we shouldn't find it now
        accountDao.queryForId(account.id).subscribe {
            println("count ${Account.NAME_COLUMN_NAME} LIKE hello: $it")
        }
    }
}
